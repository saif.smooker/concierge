@extends('layouts.non-auth')

@section('content')
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 p-5">
                                <div class="mx-auto mb-5">
                                    <a href="/">
                                        <img src="assets/images/logo.png" alt="" height="24" />
                                    </a>
                                </div>

                                <h6 class="h5 mb-0 mt-4">Créer un nouveau compte</h6>
                                <p class="text-muted mt-0 mb-4">Créer un compte particulier ou société</p>

                                @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>
                                <br>@endif
                                @if(session('success'))<div class="alert alert-success">{{ session('success') }}</div>
                                <br>@endif

                                <form method="POST" action="{{ route('register') }}" class="authentication-form">
                                    @csrf

                                    <div class="form-group">
                                        <label class="form-control-label">Nom Complet</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="user"></i>
                                                </span>
                                            </div>
                                            <input type="text"
                                                name="name" value="{{ old('name')}}"
                                                class="form-control @if($errors->has('name')) is-invalid @endif"
                                                id="name" placeholder="Nom Complet" />

                                            @if($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Adresse Email</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="mail"></i>
                                                </span>
                                            </div>
                                            <input type="email"
                                                name="email" value="{{ old('email')}}"
                                                class="form-control @if($errors->has('email')) is-invalid @endif"
                                                id="email" placeholder="Votre@Email.com" />

                                            @if($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="form-control-label">Mot de passe</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="lock"></i>
                                                </span>
                                            </div>
                                            <input type="password"
                                                name="password"
                                                class="form-control @if($errors->has('password')) is-invalid @endif"
                                                id="password" placeholder="*******" />

                                            @if($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="form-control-label">Confirmation de mot de passe</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="lock"></i>
                                                </span>
                                            </div>
                                            <input type="password"
                                                name="confirm_password"
                                                class="form-control @if($errors->has('confirm_password')) is-invalid @endif"
                                                id="confirm_password" placeholder="*******" />

                                            @if($errors->has('confirm_password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('confirm_password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Addresse</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="home"></i>
                                                </span>
                                            </div>
                                            <input type="text"
                                                   name="adresse" value="{{ old('adresse')}}"
                                                   class="form-control @if($errors->has('adresse')) is-invalid @endif"
                                                   id="address" placeholder="Votre adresse" />

                                            @if($errors->has('adresse'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('adresse') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Téléphone</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="phone"></i>
                                                </span>
                                            </div>
                                            <input type="text"
                                                   name="phone" value="{{ old('phone')}}"
                                                   class="form-control @if($errors->has('phone')) is-invalid @endif"
                                                   id="phone" placeholder="Numero de Télephone" />

                                            @if($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group d-flex">
                                        <div class="form-check ">
                                            <input class="form-check-input" name="role" type="radio" value="company" id="company" checked>
                                            <label class="form-check-label" for="company">
                                                Société
                                            </label>
                                        </div>

                                        <div class="form-check ml-3">
                                            <input class="form-check-input" name="role" type="radio" value="particular" id="perticulier">
                                            <label class="form-check-label" for="perticulier">
                                                Particulier
                                            </label>
                                        </div>
                                    </div>

                                    <div class="additional-inputs">

                                        <div class="form-group">
                                            <label class="form-control-label">Nombre Employées</label>
                                            <div class="input-group input-group-merge">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="icon-dual" data-feather="users"></i>
                                                </span>
                                                </div>
                                                <input type="number"
                                                       name="nbr_employee" value="{{ old('nbr_employee') ?  old('nbr_employee') : 1}}" min="1"
                                                       class="form-control @if($errors->has('nbr_employee')) is-invalid @endif"
                                                       id="nbr_employee" />

                                                @if($errors->has('nbr_employee'))
                                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nbr_employee') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit">S'inscrire</button>
                                    </div>

                                </form>
                            </div>

                            <div class="col-lg-6 d-none d-md-inline-block">
                                <div class="auth-page-sidebar">
                                    <div class="overlay"></div>
                                    <div class="auth-user-testimonial">
                                        <p class="font-size-24 font-weight-bold text-white mb-1">Equipe Rodina!

                                            </p>
                                        <p class="lead">""EXPÉRIENCE CLIENT ET ADAPTATION AU BESOIN!"!</i>
                                        </p>
{{--                                        <p>- Admin User</p>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p class="text-muted">Already have account? <a href="/login"
                                class="text-primary font-weight-bold ml-1">Login</a></p>
                    </div> <!-- end col -->
                </div>
                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p class="text-muted"> <a href="{{route('home')}}"
                                class="text-primary font-weight-bold ml-1">Page d'Accueil</a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('input[name=role]').change(function () {
                if ($(this).val() === 'company') {
                    $('.additional-inputs').show();
                } else if ($(this).val() === 'particular') {
                    $('.additional-inputs').hide();
                }
            })
        })
    </script>
@endsection
