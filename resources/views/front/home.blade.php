@extends('layouts.front')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('front/vendor/iziToast/iziToast.min.css')}}">
    <!-- Revolution Slider 5.x CSS settings -->
    <link  href="{{asset('assets/libs/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
    <link  href="{{asset('assets/libs/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
    <link  href="{{asset('assets/libs/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .btn-theme-colored2 {
            background-color: #2cdd9b;
            border-color: #00BBD1;
        }
        .btn-transparent {
            background-color: transparent;
            color: #fff;
        }
        .btn-default:hover {
            color: #333;
            background-color: #e6e6e6;
            border-color: #adadad;
        }
        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
    </style>
@endsection
@section('content')

    <!-- START REVOLUTION SLIDER 5.0.7 -->
    <div id="rev_slider_home_wrapper" class="rev_slider_wrapper" data-alias="news-gallery34" style="margin:0px auto; background-color:#ffffff; padding:0px; margin-top:0px; margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
        <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>
                @foreach($sliders as $slider)
                <!-- SLIDE 1 -->
                <li data-index="rs-{{$slider->id}}" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{asset('storage/pictures/slider/'.$slider->image)}}" data-rotate="0"  data-fstransition="fade" data-saveperformance="off" data-title="Web Show" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('storage/pictures/slider/'.$slider->image)}}" alt="" data-bgposition="center 30%" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0 bg-theme-colored-transparent-1"
                         id="slide-1-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-start="500"
                         data-basealign="slide"
                         data-responsive_offset="on"
                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 text-white text-uppercase font-roboto-slab font-weight-700"
                         id="slide-1-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['170','150','135','135']"
                         data-fontsize="['32','28','22','20']"
                         data-lineheight="['56','50','46','40']"
                         data-fontweight="['800','700','700','700']"
                         data-textalign="['center','center','center','center']"
                         data-width="['700','650','600','420']"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                         data-start="600"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 5; white-space: nowrap; font-weight:700;">{{$slider->title}}
                    </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 text-white text-uppercase font-roboto-slab font-weight-700"
                         id="slide-1-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['225','195','175','175']"
                         data-fontsize="['60','52','42','36']"
                         data-lineheight="['70','60','50','45']"
                         data-fontweight="['800','700','700','700']"
                         data-textalign="['center','center','center','center']"
                         data-width="['700','650','600','420']"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                         data-start="600"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 5; white-space: nowrap; font-weight:700;"> <span class="text-theme-colored2">{{$slider->subtitle}}</span>
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0"
                         id="slide-1-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['305','260','225','225']"
                         data-fontsize="['15','15',14',16']"
                         data-lineheight="['22','24','24','24']"
                         data-fontweight="['400','400','400','400']"
                         data-textalign="['center','center','center','center']"
                         data-width="['800','650','600','460']"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                         data-start="700"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 5; white-space: nowrap;">{{$slider->description}}</div>
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption rs-parallaxlevel-0"
                         id="slide-1-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['370','330','290','290']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;"
                         data-mask_out="x:0;y:0;"
                         data-start="800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-responsive="off"
                         style="z-index: 5; white-space: nowrap; letter-spacing:1px;">
                        <a class="btn btn-theme-colored2 btn-lg btn-flat text-white font-weight-600 pl-30 pr-30 mr-15" href="{{$slider->button_link ? $slider->button_link : ''}}">{{$slider->button_text}}</a>
                        <a class="btn btn-default btn-transparent btn-bordered btn-lg btn-flat font-weight-600 pl-30 pr-30" href="{{$slider->button_link2 ? $slider->button_link2 : ''}}">{{$slider->button_text2}}</a>
                    </div>
                </li>
                    @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(255, 255, 255, 0.2);"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->

    <!-- Begin Page Content -->
{{--    <section class="py-5 homepage-search-block position-relative">--}}
{{--        <div class="container">--}}
{{--            <div class="row py-lg-5">--}}
{{--                <div class="col-lg-5">--}}
{{--                    <img class="img-fluid" src="{{asset('front/images/banner.svg')}}" alt='' />--}}
{{--                </div>--}}
{{--                <div class="col-lg-7">--}}
{{--                    <div class="homepage-search-title">--}}
{{--                        <h1 class="mb-3 text-shadow text-gray-900 font-weight-bold">Trouvez les services parfaits pour votre entreprise</h1>--}}
{{--                        <h5 class="mb-5 text-shadow text-gray-800 font-weight-normal">Beaucoup de gens utilisent notre site Web pour faciliter leur vie.--}}
{{--                        </h5>--}}
{{--                    </div>--}}
{{--                    <div class="homepage-search-form">--}}
{{--                        <form class="form-noborder">--}}
{{--                            <div class="form-row">--}}
{{--                                <div class="col-lg-3 col-md-3 col-sm-12 form-group">--}}
{{--                                    <div class="location-dropdown">--}}
{{--                                        <i class="icofont-location-arrow"></i>--}}
{{--                                        <select class="custom-select form-control border-0 shadow-sm form-control-lg" id="categories-select">--}}
{{--                                            <option> Tout les Catégories </option>--}}
{{--                                            @foreach($categories as $category)--}}
{{--                                                <option value="{{$category->id}}">{{$category->name}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-7 col-md-7 col-sm-12 form-group">--}}
{{--                                    <select name="service" id="select-sercive" class="form-control border-0 form-control-lg shadow-sm">--}}
{{--                                        <option value="" selected>--}}
{{--                                            Select Service--}}
{{--                                        </option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-2 col-md-2 col-sm-12 form-group">--}}
{{--                                    <a id="href" href=""--}}
{{--                                        class="btn btn-success btn-block btn-lg btn-gradient shadow-sm"><i--}}
{{--                                            class="text-white fa fa-search"></i></a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <section class="py-5 bg-white">

        <div class="container">
            <div class="mb-4">
                <h2 class="text-center text-shadow text-gray-900 font-weight-bold">A votre service</h2>
                <h4 class="text-center text-shadow text-gray-800 font-weight-normal">Que fait-on pour vous aujourd'hui ?</h4>
            </div>

            <div class="row service-slider">
                @foreach($categories as $category)
                    <div class="col">
                        <div class="service">
                            <a href="{{route('service.list',$category->id)}}">
                                <img style="height: 282px!important;width: 327px!important;" src="{{asset('storage/pictures/category/'.$category->id.'/'.$category->image)}}">
                                <h3><span></span>{{$category->name}} </h3>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </section>
    <!--       recent -->

    <!--    about section -->
    <div class="container">
        <div class="about-section py-5">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2>COMMENT ÇA MARCHE ?
                        , <br>Libérez votre quotidien en 1 clic
                    </h2>
                    <ul>
                        <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Commande</span>
                            24h/7j depuis Conciergerie
                        </li>
                        <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Dépôt et Retrait
                        </span>
                            De vos articles dans les casiers connectés en libre service
                        </li>
                        <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Livraison</span>Dans votre résidence et lieu de travail
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('front/images/video.svg')}}" class="video-img w-100">
                </div>
            </div>
        </div>
    </div>
    <!--    about section -->

    <!--       testimonials -->
    <div class="testi-wrap">
        <div class="container">
            <div class="testimonial">
                <div class="video-modal">
                    <div class="picture-wrapper">
                        <img src="{{asset('front/images/1440-haerfest-2x.jpg')}}">
                    </div>
                </div>
                <div class="text-content">
                    <p>Commandez vos services depuis à domicile
                    </p>
                    {{--                    <span>Tim and Dan Joo, Co-founders</span>--}}
                    <img alt="Company logo" src="{{asset('front/images/haerfest-logo.png')}}" loading="lazy">
                </div>
            </div>
        </div>
    </div>
    <!--       testimonials -->

    <!--       pro-wrapper -->
    <div class="container py-5">
        <div class="pro-section">

        </div>
    </div>
    <!--       pro-wrapper -->

    <!--    about section -->
    <section class="bg-white">
        <div class="container py-5">
            <div class="about-section">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2>Donnez à votre entreprise les bons outils</h2>
                        <ul>
                            <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Remplir Une Demande</span> A new shared dashboard allows you to track your team's activity, so that everyone is always in sync.</li>
                            <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Attendre Une Réponse
                           </span>Quick response time and upgraded support solutions will help you to get what you need, when you need it.
                            </li>
                            <li><span><img src="{{asset('front/images/checkmark.svg')}}"> Commandez Vos Service</span>One team - one payment method. You can now add a card on file that every team member can use.</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <img src="{{asset('front/images/ipad.svg')}}" class="video-img w-100">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    about section -->

    @if(!Auth::user())
    <!-- get started -->
    <div>
        <div class="get-started">
            <div class="content">
                <h2>Trouvez des services indépendants pour votre entreprise dès aujourd'hui</h2>
                <p>Remplir Votre Demande d'Inscription</p>
                <a href="/register" class="c-btn c-fill-color-btn">Rejoignez nous</a>
            </div>
        </div>
    </div>
    <!-- get started -->
    @endif
@endsection
@section('js')
    <script src="{{asset('assets/libs/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('assets/libs/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript">
        var tpj=jQuery;
        var revapi34;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
                revapi34 = tpj("#rev_slider_home").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"{{asset('assets/libs/revolution-slider/js')}}",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:5000,
                    navigation: {
                        keyboardNavigation:"on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        onHoverStop:"on",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"zeus",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:600,
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            }
                        },
                        bullets: {
                            enable:true,
                            hide_onmobile:true,
                            hide_under:600,
                            style:"metis",
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            direction:"horizontal",
                            h_align:"center",
                            v_align:"bottom",
                            h_offset:0,
                            v_offset:30,
                            space:5,
                            tmp:'<span class="tp-bullet-img-wrap"><span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable:true,
                        outof:"pause",
                        visible_area:"80%"
                    },
                    responsiveLevels:[1240,1024,778,480],
                    gridwidth:[1240,1024,778,480],
                    gridheight:[600,550,500,450],
                    lazyType:"none",
                    parallax: {
                        type:"scroll",
                        origo:"enterpoint",
                        speed:400,
                        levels:[5,10,15,20,25,30,35,40,45,50],
                    },
                    shadow:0,
                    spinner:"off",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"off",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        }); /*ready*/
    </script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/libs/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{asset('front/vendor/iziToast/iziToast.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('body').on('change','#categories-select', function () {
                $.ajax({
                    url : "{{route('ajax.getservices')}}",
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        category_id: $(this).val(),
                    },
                    success: function(response) {
                        $('#select-sercive').html(response);
                    }
                });
            });
            $('body').on('change','#select-sercive', function () {
                $.ajax({
                    success: function(response) {
                        $('#href').prop("href", "service/"+$('#select-sercive').val());
                    }
                });
            });
        })
    </script>
    <script>
        @if($errors->all())
        @foreach($errors->all() as $message)
        iziToast.error({
            title: 'Erreur',
            message: '{{ $message }}',
            position: 'bottomRight'
        });
        @endforeach

        @elseif(session()->has('message'))
        iziToast.success({
            title: 'Succès',
            message: '{{ session()->get('message') }}',
            position: 'bottomRight'
        });

        @elseif(session()->has('error'))
        iziToast.error({
            title: 'Erreur',
            message: '{{ session()->get('error') }}',
            position: 'bottomRight'
        });
        @endif
    </script>
@endsection
