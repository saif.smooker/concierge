@extends('layouts.front')
@section('content')
    <!-- About -->
    <section class="py-5 bg-white">
        <div class="container">
            <div class="row align-items-center">
                <div class="pl-4 col-lg-5 col-md-5 pr-4">
                    <img class="rounded img-fluid" src="{{asset('front/images/about.jpg')}}" alt="Card image cap">
                </div>
                <div class="col-lg-6 col-md-6 pl-5 pr-5">
                    <h2 class="mb-5">QUI SOMMES NOUS ?
                    </h2>
                    <h5 class="mt-2">Our Vision</h5>
                    <p>
                        RODINA FACILITIES & LOGISTIC’S, est une société spécialisé dans la gestion
                        globale de votre projet de logistique et de conciergerie.
                        A travers les prestations proposées, RODINA FACILITIES & LOGISTIC’S, met à
                        votre disposition une boite à outils de solutions permettant de répondre
                        au mieux à vos besoins.
                        Nos solutions professionnelles proposées, vous permettent de vous désengager des soucis au quotidien et vous concentrer sur votre cœur de
                        métier.
                        Que vous soyez une entreprise désirant de simplifier la vie au quotidien
                        de vos salariés, un professionnel libérale ou un e-commerçant, RODINA
                        FACILITIES & LOGISTIC’S vous offre la possibilité de gagner en temps et en
                        argent en adoptant une nouvelle formule de gestion.

                    </p>
                    <h5 class="mt-4">Our Goal</h5>
                    <p>
                        RODINA CONCIERGERIE est une conciergerie connectée, sous
                        forme d’une plateforme internet spécialisée dans les services du
                        quotidien pour aider les salariés de votre entreprise ou adhérents
                        de vos corporations ou amicales à résoudre " LES PROBLÈMES DU
                        QUOTIDIEN " : pressing, cordonnerie, repassage, révision automobile,
                        courses, pharmacie etc...
                        Notre équipe de professionnels répond aux demandes "privées"
                        de vos collaborateurs en offrant une gamme de services avec les
                        meilleurs prestataires du marché.
                        De multiples services pratiques à la disposition de vos salariés
                        directement sur leur lieu de travail.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End About -->
    <!-- What We Provide -->
    <section class="py-5">
        <div class="section-title text-center mb-5">
            <h2>Que fournissons-nous?</h2>
            <p class="container">Si votre Entreprise, votre Comité d’Entreprise, votre corporations ou
                même votre amicale met en place un système de conciergerie pour
                ses salariés ou ses adhérents, vous pouvez alors accéder, sans surcoût,
                sans engagement et en toute liberté à notre panoplie de services.
            </p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-account-box-outline mdi-48px"></i></div>
                    <h5 class="mt-3 mb-3">ECOUTE</h5>
                    <p>Nos clients (vos salariés ou vos adhérents) sont notre priorité. Nous
                        oeuvrons quotidiennement pour leur satisfaction</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-check-circle-outline mdi-48px"></i></div>
                    <h5 class="mb-3">CONFIANCE</h5>
                    <p>Grâce à une qualité de service irréprochable et à des solutions innovantes,
                        nous forgeons avec nos clients des partenariats basés sur la confiance..</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-account-multiple-outline mdi-48px"></i></div>
                    <h5 class="mt-3 mb-3">PROXIMITÉ</h5>
                    <p>Proches de nos clients comme de nos partenaires locaux; nous sommes
                        disponibles et réactifs afin de répondre à leurs attentes.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-clock mdi-48px"></i></div>
                    <h5 class="mb-3">TRANSPARENCE</h5>
                    <p>La richesse des échanges, facteur de créativité et d’innovation,
                        bénéficie toujours aux clients.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-sticker-emoji mdi-48px"></i></div>
                    <h5 class="mt-3 mb-3">POLITIQUE RSE</h5>
                    <p>On s’engage à collaborer qu’avec les associations et prestataires
                        de notre marché Tunisien</p>
                </div>
{{--                <div class="col-lg-4 col-md-4">--}}
{{--                    <div class="mt-4 mb-4"><i class="text-success mdi mdi-comment-alert-outline mdi-48px"></i></div>--}}
{{--                    <h5 class="mt-3 mb-3">Help</h5>--}}
{{--                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,</p>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End What We Provide -->
    <!-- Trusted Agents -->
{{--    <section class="py-5 bg-white">--}}
{{--        <div class="section-title text-center mb-5">--}}
{{--            <h2>Trusted Agents</h2>--}}
{{--            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
{{--        </div>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-4 col-md-4">--}}
{{--                    <div class="agents-card text-center">--}}
{{--                        <img class="img-fluid mb-4" src="images/user/s1.png" alt="">--}}
{{--                        <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>--}}
{{--                        <h6 class="mb-0 text-success">- Stave Martin</h6>--}}
{{--                        <small>Buying Agent</small>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-4">--}}
{{--                    <div class="agents-card text-center">--}}
{{--                        <img class="img-fluid mb-4" src="images/user/s2.png" alt="">--}}
{{--                        <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>--}}
{{--                        <h6 class="mb-0 text-success">- Mark Smith</h6>--}}
{{--                        <small>Selling Agent</small>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-4">--}}
{{--                    <div class="agents-card text-center">--}}
{{--                        <img class="img-fluid mb-4" src="images/user/s3.png" alt="">--}}
{{--                        <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>--}}
{{--                        <h6 class="mb-0 text-success">- Ryan Printz</h6>--}}
{{--                        <small>Real Estate Broker</small>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Trusted Agents -->
@endsection
