@extends('layouts.front')
@section('css')
    <style>
        .avatar-upload {
            position: relative;
            max-width: 205px;
            margin: 1px auto;
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection
@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row">
                <!-- Main Content -->
                <aside class="col-md-4">
                    <div class="box mb-3 shadow-sm rounded bg-white profile-box text-center">
                        <form method="post" action="{{route('compte.update')}}"  enctype="multipart/form-data">
                            @csrf
                            @method('put')
                        <div class="py-4 px-3 border-bottom">

                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input required name="image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{asset('storage/pictures/user/'.auth()->user()->image)}});">
                                    </div>
                                </div>
                            </div>

                            <h5 class="font-weight-bold text-dark mb-1 mt-4">{{auth()->user()->name}}</h5>
                            <p class="mb-0 text-muted">
                                @hasanyrole('company|employee')
                                {{Auth::user()->company->name}}
                                @else
                                 @endhasanyrole
                            </p>
                        </div>
                        <div class="p-4">
                            <button type="submit" class="btn btn-info">Changer Image</button>
                        </div>
                      </form>
                    </div>
                </aside>
                <main class="col-md-8">
                    <div class="shadow-sm rounded bg-white mb-3">
                        <div class="box-title border-bottom p-3">
{{--                            <h6 class="m-0">Edit Basic Info</h6>--}}
{{--                            <p class="mb-0 mt-0 small">Informations--}}
{{--                            </p>--}}
                            <ul class="nav nav-pills navtab-bg nav-justified">
                                <li class="nav-item">
                                    <a href="#information" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                        <span class="d-block d-sm-none"><i class="uil-home-alt"></i></span>
                                        <span class="d-none d-sm-block">Informations générales</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#password" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                        <span class="d-block d-sm-none"><i class="uil-user"></i></span>
                                        <span class="d-none d-sm-block">Mot de passe </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content text-muted">
                            <div class="tab-pane show active" id="information">
                                <div class="box-body p-3">
                                    <form method="post" action="{{route('compte.update')}}" class="js-validate" novalidate="novalidate">
                                       @csrf
                                        @method('put')
                                        <div class="row">
                                            <!-- Input -->
                                            <div class="col-sm-6 mb-2">
                                                <div class="js-form-message">
                                                    <label id="nameLabel" class="form-label">
                                                        Name
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="name" value="{{auth()->user()->name}}" data-error-class="u-has-error" data-success-class="u-has-success">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Input -->
                                            <!-- Input -->
                                            <div class="col-sm-6 mb-2">
                                                <div class="js-form-message">
                                                    <label id="usernameLabel" class="form-label">
                                                        Email
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" name="email" value="{{auth()->user()->email}}" placeholder="Enter your username" data-error-class="u-has-error" data-success-class="u-has-success">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Input -->
                                            <!-- Input -->
                                            <div class="col-sm-6 mb-2">
                                                <div class="js-form-message">
                                                    <label id="usernameLabel" class="form-label">
                                                        Adresse
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input name="adresse" type="text" class="form-control" name="adresse" value="{{auth()->user()->adresse}}"  data-error-class="u-has-error" data-success-class="u-has-success">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Input -->
                                        </div>
                                        <div class="mb-3 text-right">
                                            <button type="submit" class="btn btn-success">Modifier</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane " id="password">
                                <div class="box-body p-3">
                                    <form class="js-validate" novalidate="novalidate">
                                        <div class="row">
                                            <!-- Input -->
                                            <div class="col-sm-6 mb-2">
                                                <div class="js-form-message">
                                                    <label id="nameLabel" class="form-label">
                                                        Mot de Passe
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input name="password" type="password" class="form-control" placeholder="Taper Votre Nouveau Mot de Passe" data-error-class="u-has-error" data-success-class="u-has-success">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Input -->
                                        </div>
                                        <div class="mb-3 text-right">
                                            <button type="submit" class="btn btn-success">Modifier</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
    @endsection
