@extends('layouts.front')
@section('css')
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .avatar-sm {
            height: 2.25rem;
            width: 2.25rem;
        }
    </style>
@endsection
@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row">
                <!-- Main Content -->
                <aside class="col col-xl-3 order-xl-1 col-lg-12 order-lg-1 col-12">
                    <div class="box mb-3 shadow-sm rounded bg-white profile-box text-center">
                        <div class="py-4 px-3 border-bottom">
                            <img src="{{!auth()->user()->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/user/'.auth()->user()->image)}}" class="img-fluid mt-2 rounded-circle" alt="Responsive image">
                            <h5 class="font-weight-bold text-dark mb-1 mt-4">{{auth()->user()->name}}</h5>
                            <p class="mb-0 text-muted">
                                @hasanyrole('company|employee')
                                {{Auth::user()->company->name}}
                                @else

                                    @endhasanyrole
                            </p>
                        </div>
                        <div class="d-flex">
                            <div class="col-6 border-right p-3">
                                <h6 class="font-weight-bold text-dark mb-1">{{Auth::user()->orders->count()}}</h6>
                                <p class="mb-0 text-black-50 small">Commandes</p>
                            </div>
                            <div class="col-6 p-3">
                                <h6 class="font-weight-bold text-dark mb-1">85</h6>
                                <p class="mb-0 text-black-50 small">Favoris</p>
                            </div>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="{{route('compte.edit')}}">
                                Modifier mon Profil
                            </a>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="">
                                Se Déconnecter
                            </a>
                        </div>
                    </div>

                </aside>
                <main class="col col-xl-9 order-xl-2 col-lg-12 order-lg-2 col-md-12 col-sm-12 col-12">
                    <div class="box shadow-sm rounded bg-white mb-3">
                        <div class="box-title border-bottom p-3">
                            <h6 class="m-0">Ma Commande</h6>
                        </div>
                        <ul class="nav nav-pills navtab-bg nav-justified">
                            <li class="nav-item">
                                <a href="#commande" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                    <span class="d-block d-sm-none"><i class="uil-home-alt"></i></span>
                                    <span class="d-none d-sm-block">Devis</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#facture" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                    <span class="d-block d-sm-none"><i class="uil-user"></i></span>
                                    <span class="d-none d-sm-block">Documents & Factures</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content text-muted">
                            <div class="tab-pane show active" id="commande">
                                <div class="box-body p-3">
                            <div class="card">
                                <div class="card-body">
                                    <!-- Logo & title -->
                                    <div class="clearfix">
                                        <div class="float-sm-right">
                                            <img src="{{asset('assets/images/logo.png')}}" alt="" height="48" />
                                            <h4 class="m-0 d-inline align-middle">Rodina</h4>
                                            <address class="pl-2 mt-2">

                                                <abbr title="Phone">Tel:</abbr> (123) 456-7890
                                            </address>
                                        </div>
                                        <div class="float-sm-left">
                                            <h4 class="m-0 d-print-none">Commande</h4>
                                            <dl class="row mb-2 mt-3">
                                                <dt class="col-sm-3 font-weight-normal">Numéro Commande :</dt>
                                                <dd class="col-sm-9 font-weight-normal">#{{$order->code}}</dd>

                                                <dt class="col-sm-3 font-weight-normal"> Date Création :</dt>
                                                <dd class="col-sm-9 font-weight-normal">{{$order->created_at}}</dd>

                                                <dt class="col-sm-3 font-weight-normal">Date Réservation :</dt>
                                                <dd class="col-sm-9 font-weight-normal">{{$order->datedeb}} {{$order->datefin ? 'à'.' '.$order->datefin : ''}} </dd>
                                            </dl>
                                        </div>

                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-6">
                                            <h6 class="font-weight-normal">Commande Pour:</h6>
                                            <h6 class="font-size-16">{{$order->user->name}}</h6>
                                            <address>
                                                {{Auth::user()->adresse}}
{{--                                                <abbr title="Phone">Tel:</abbr> {{$user->phone}}--}}
                                            </address>
                                        </div> <!-- end col -->

{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="text-md-right">--}}
{{--                                                <h6 class="font-weight-normal">Total</h6>--}}
{{--                                                <h2>{{$order->price}}</h2>--}}
{{--                                            </div>--}}
{{--                                        </div> <!-- end col -->--}}
                                    </div>
                                    <!-- end row -->
                                    @if($order->product)
                                        <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table mt-4 table-centered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nom</th>
                                                        <th style="width: 10%">Prix Unitaire</th>
                                                        <th style="width: 10%">Quantité</th>
                                                        <th style="width: 10%" class="text-right">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($products as $product)
                                                        <tr>
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>
                                                            <h5 class="font-size-16 mt-0 mb-2">{{App\Product::find($product->id)->name}}</h5>
                                                            <p class="text-muted mb-0">{{App\Product::find($product->id)->description}}</p>
                                                        </td>
                                                        <td>{{App\Product::find($product->id)->price}}</td>
                                                        <td>{{$product->qty}}</td>
                                                        <td class="text-right">{{$product->price*$product->qty}}</td>
                                                    </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div> <!-- end table-responsive -->
                                        </div> <!-- end col -->
                                    </div>
                                    @endif
                                    <!-- end row -->

                                    <div class="row">
                                        @if($order->besoin)
                                        <div class="col-sm-6">
                                            <div class="clearfix pt-5">
                                                <h6 class="text-muted">Besoin:</h6>

                                                <small class="text-muted">
                                                    {{$order->besoin}}
                                                </small>
                                            </div>
                                        </div> <!-- end col -->
                                        @endif
                                        <div class="col-sm-6">
                                            <div class="float-right mt-4">
{{--                                                <p><span class="font-weight-medium">total:</span> <span--}}
{{--                                                        class="float-right">{{$total}}</span></p>--}}
{{--                                                <p><span class="font-weight-medium">Discount (10%):</span> <span class="float-right">--}}
{{--                                    &nbsp;&nbsp;&nbsp; $459.75</span></p>--}}
                                                <h3>{{$total}} DT</h3>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div> <!-- end col -->
                                    </div>
                                    <!-- end row -->

                                    <div class="mt-5 mb-1">
                                        <div class="text-right d-print-none">
                                            <a href="javascript:window.print()" class="btn btn-primary"><i class="uil uil-print mr-1"></i>
                                                Print</a>
                                            <a href="#" class="btn btn-info">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            <div class="tab-pane " id="facture">
                                <div class="box-body p-3">
                                    <h5 class="mt-3">Mes Factures({{$order->bills->count()}})</h5>
                                    @foreach($order->bills as $facture)
                                        {{--                                        <a href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}">Document{{$loop->iteration}}</a>--}}
                                        <div class="card mb-2 shadow-none border">
                                            <div class="p-1 px-2">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <img src="{{asset('assets/images/logo.png')}}" class="avatar-sm rounded" alt="file-image">
                                                    </div>
                                                    <div class="col pl-0">
                                                        <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/facture/'.$facture->name)}}" class="text-muted font-weight-bold">{{$loop->iteration}}</a>
                                                        {{--                                                        <p class="mb-0">2.3 MB</p>--}}
                                                    </div>
                                                    <div class="col-auto">
                                                        <!-- Button -->
                                                        <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/facture/'.$facture->name)}}" data-toggle="tooltip" data-placement="bottom" title="" class="btn btn-link text-muted btn-lg p-0" data-original-title="Download">
                                                            <i class="uil uil-cloud-download font-size-14"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <h5 class="mt-3">Mes Documents({{$order->documents->count()}})</h5>
                                    @foreach($order->documents as $document)
{{--                                        <a href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}">Document{{$loop->iteration}}</a>--}}
                                        <div class="card mb-2 shadow-none border">
                                            <div class="p-1 px-2">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <img src="{{asset('assets/images/projects/project-1.jpg')}}" class="avatar-sm rounded" alt="file-image">
                                                    </div>
                                                    <div class="col pl-0">
                                                        <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}" class="text-muted font-weight-bold">{{$document->name}}</a>
{{--                                                        <p class="mb-0">2.3 MB</p>--}}
                                                    </div>
                                                    <div class="col-auto">
                                                        <!-- Button -->
                                                        <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}" data-toggle="tooltip" data-placement="bottom" title="" class="btn btn-link text-muted btn-lg p-0" data-original-title="Download">
                                                            <i class="uil uil-cloud-download font-size-14"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        </div>

                </main>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script>
        // function activaTab(tab){
        //     $('.nav-pills a[href="#' + tab + '"]').tab('show');
        // };
    </script>
@endsection
