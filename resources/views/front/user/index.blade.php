@extends('layouts.front')
@section('css')
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row">
                <!-- Main Content -->
                <aside class="col col-xl-3 order-xl-1 col-lg-12 order-lg-1 col-12">
                    <div class="box mb-3 shadow-sm rounded bg-white profile-box text-center">
                        <div class="py-4 px-3 border-bottom">
                            <img src="{{!auth()->user()->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/user/'.auth()->user()->image)}}" class="img-fluid mt-2 rounded-circle" alt="Responsive image">
                            <h5 class="font-weight-bold text-dark mb-1 mt-4">{{auth()->user()->name}}</h5>
                            <p class="mb-0 text-muted">
                            @hasanyrole('company|employee')
                                {{Auth::user()->company->name}}
                                @else

                             @endhasanyrole
                            </p>
                        </div>
                        <div class="d-flex">
                            <div class="col-6 border-right p-3">
                                <h6 class="font-weight-bold text-dark mb-1">{{auth()->user()->orders->count()}}</h6>
                                <p class="mb-0 text-black-50 small">Commandes</p>
                            </div>
                            <div class="col-6 p-3">
                                <h6 class="font-weight-bold text-dark mb-1">85</h6>
                                <p class="mb-0 text-black-50 small">Favoris</p>
                            </div>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="{{route('compte.edit')}}">
                                Modifier mon Profil
                            </a>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="{{route('compte')}}">
                                Mes Commandes
                            </a>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="{{route('home')}}/tickets">
                                Mes Réclamations
                            </a>
                        </div>
                        <div class="overflow-hidden border-top">
                            <a class="font-weight-bold p-3 d-block" href="{{route('logout')}}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                                Se Déconnecter
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>

                </aside>
                <main class="col col-xl-9 order-xl-2 col-lg-12 order-lg-2 col-md-12 col-sm-12 col-12">
                    <div class="box shadow-sm rounded bg-white mb-3">
                        <div class="box-title border-bottom p-3">
                            <h6 class="m-0">Mes Commandes</h6>
                        </div>
                        <div class="box-body p-3 scroll-mobile">
                            <table id="test" class="table nowrap table-hover">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Date</th>
                                    <th>Service</th>
                                    <th>Prix</th>
                                    <th>Statut</th>
                                    <th>Voir</th>
                                </tr>
                                </thead>

                                <tbody>
{{--                                @foreach($orders as $order)--}}
{{--                                    <tr>--}}

{{--                                        <td>{{$order->code}}</td>--}}
{{--                                        <td>{{$order->created_at->diffForHumans()}}</td>--}}
{{--                                        <td>{{$order->service->name}}</td>--}}
{{--                                        <td>--}}
{{--                                            @if($order->product)--}}
{{--                                                 {{$order->total()}}DT--}}
{{--                                            @endif--}}
{{--                                            @if($order->facture)--}}
{{--                                                    <button class="btn btn-light btn-sm" data-toggle="modal"--}}
{{--                                                            data-target="#modal-facture{{$order->id}}">Voir Facture</button>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if($order->status_id==1)--}}
{{--                                            <span class="badge badge-warning" >En attente</span>--}}
{{--                                            @elseif($order->status_id==2)--}}
{{--                                            <span  class="badge badge-info" >En cours</span>--}}
{{--                                            @else--}}
{{--                                            <span class="badge badge-success" >Livré</span>--}}
{{--                                                @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a class="btn btn-light btn-sm" href="{{route('compte.order',$order->id)}}">Voir Commande</a>--}}
{{--                                        </td>--}}

{{--                                    </tr>--}}
{{--                                    <div class="modal fade" id="modal-facture{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"--}}
{{--                                         aria-hidden="true">--}}
{{--                                        <div class="modal-dialog modal-lg modal-dialog-centered">--}}
{{--                                            <div class="modal-content">--}}
{{--                                                <div class="modal-header">--}}
{{--                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                        <span aria-hidden="true">&times;</span>--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-body text-center">--}}
{{--                                                    <div class="mt-4">--}}
{{--                                                        <div class="row d-flex justify-content-center">--}}
{{--                                                            <div class="col-md-12">--}}
{{--                                                                <img src="{{asset('storage/pictures/order/'.$order->id.'/facture/'.$order->facture)}}">--}}
{{--                                                            </div>--}}

{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div><!-- /.modal-content -->--}}
{{--                                        </div><!-- /.modal-dialog -->--}}
{{--                                    </div><!-- /.modal -->--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
{{--    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>--}}
    <script>
        $(function () {

            var table = $('#test').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('compte') }}",
                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'date', name: 'date'},
                    {data: 'name', name: 'name'},
                    {data: 'prix', name: 'prix'},
                    {data: 'status', name: 'status'},
                    {
                        data: 'action',
                        name: 'action',
                        searchable: true
                    },

                ],
                responsive:true,
                order:[0,'desc'],
                "language": {
                    "emptyTable": "Aucune donnée disponible dans le tableau",
                    "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                    "infoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
                    "infoFiltered": "(filtré à partir de _MAX_ éléments au total)",
                    "infoThousands": ",",
                    "lengthMenu": "Afficher _MENU_ éléments",
                    "loadingRecords": "Chargement...",
                    "processing": "Traitement...",
                    "search": "Rechercher :",
                    "zeroRecords": "Aucun élément correspondant trouvé",
                    "paginate": {
                        "first": "Premier",
                        "last": "Dernier",
                        "next": "Suivant",
                        "previous": "Précédent"
                    },
                }

            });

        });
    </script>
@endsection
