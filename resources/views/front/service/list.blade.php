@extends('layouts.front')
@section('content')
    <!-- Inner Header -->
    <section class="py-5  inner-header" style="background-image: url({{asset('storage/pictures/category/'.$category->id.'/'.$category->image)}});background-size:contain; ">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="mt-0 mb-3 text-white" style="text-transform: capitalize;">{{$category->name}}</h1>
                    <div class="breadcrumbs">
                        <p class="mb-0 text-white"><a class="text-white" href="{{route('home')}}">Accueil</a>  /  <span class="text-success">{{$category->name}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Inner Header -->
    <!--   header -->
    <div class="third-menu filter-options py-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center justify-content-between">
                    <div class="left">
                        <div class="dropdown-filters d-flex">
                            <div class="dropdown ml-4">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="delivery" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Catégories
                                </button>
                                <div class="dropdown-menu delivery" aria-labelledby="delivery">
                                    <div class="options">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="fake-radio-wrapper">
                                                    @foreach($categories as $category)
                                                   <a href="{{route('service.list',$category->id)}}" class="fake-radio">
                                                       {{$category->name}}
                                                   </a>
                                                        @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="main-page best-selling">
        <div class="view_slider recommended pt-5">
            <div class="container">
                <div class="sorting-div d-flex align-items-center justify-content-between">
                    <p class="mb-2">{{count($services)}} Service(s)</p>
{{--                    <div class="sorting d-flex align-items-center">--}}
{{--                        <p>Sortby</p>--}}
{{--                        <select class="custom-select custom-select-sm border-0 shadow-sm ml-2">--}}
{{--                            <option>Best Selling</option>--}}
{{--                            <option>Recommended</option>--}}
{{--                            <option>Newest Arrivals</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                </div>
                <h3>Services Dans {{$category->name}}</h3>
            </div>
            <div class="container">
                <div class="row">
                    @foreach($services as $service)
                    <div class="col-md-3">

                        <a href="{{route('service.detail',$service->id)}}" >
                        @if(!Auth::user()) <h3 style="padding: 0;"><span class="badge badge-danger">Accés restreint</span></h3>@endif
                            <img class="img-fluid" src="{{asset('storage/pictures/service/'.$service->id.'/'.$service->image)}}" />

                        </a>
                        <div class="inner-slider">
                            <div class="inner-wrapper">
                                <div class="d-flex align-items-center">
                              <span class="seller-image">
                              <img class="img-fluid"
                                   src="{{asset('storage/pictures/service/'.$service->id.'/'.$service->image)}}"
                                   alt='' />
                              </span>
                                    <span class="seller-name">
                              <a @hasrole('company|worker|employee|admin') href="{{route('service.detail',$service->id)}}" @endhasrole>{{$service->name}}</a>
                              <span class="level hint--top level-one-seller">
                              {{$service->category->name}}
                              </span>
                              </span>
                                </div>
                                <h3>
                                    {!! Str::substr($service->description,0,20) !!}...
                                </h3>
                                <div class="content-info">
                                    <div class="rating-wrapper">
{{--                                 <span class="gig-rating text-body-2">--}}
{{--                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="15" height="15">--}}
{{--                                       <path fill="currentColor"--}}
{{--                                             d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z">--}}
{{--                                       </path>--}}
{{--                                    </svg>--}}
{{--                                    5.0--}}
{{--                                    <span>(7)</span>--}}
{{--                                 </span>--}}
                                    </div>
                                </div>
{{--                                <div class="footer">--}}
{{--                                    <i class="fa fa-heart" aria-hidden="true"></i>--}}
{{--                                    <div class="price">--}}
{{--                                        <a href="product.html#">--}}
{{--                                            Starting At <span> $1,205</span>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
{{--        <div class="footer-pagination text-center">--}}
{{--            <nav aria-label="Page navigation example">--}}
{{--                <ul class="pagination">--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="product.html#" aria-label="Previous">--}}
{{--                            <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>--}}
{{--                            <!--                    <span class="sr-only"></span>-->--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item"><a class="page-link" href="product.html#">1</a></li>--}}
{{--                    <li class="page-item active"><a class="page-link" href="product.html#">2</a></li>--}}
{{--                    <li class="page-item"><a class="page-link" href="product.html#">4</a></li>--}}
{{--                    <li class="page-item"><a class="page-link" href="product.html#">5</a></li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="product.html#" aria-label="Next">--}}
{{--                            <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>--}}
{{--                            <!--                    <span class="sr-only"></span>-->--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </nav>--}}
{{--        </div>--}}
    </div>
@endsection
