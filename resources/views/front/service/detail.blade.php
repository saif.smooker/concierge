@extends('layouts.front')
@section('css')
    @if($service->multidate==1)
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    @else
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css" />
    @endif
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <link href="{{asset('front/vendor/filer/jquery.filer.css')}}" media="all" rel="stylesheet" type="text/css" />
    <style>
        .avatar-lg {
            height: 4.5rem;
            width: 4.5rem;
        }

        .input-number {
            width: 80px;
            padding: 0 12px;
            vertical-align: top;
            text-align: center;
            outline: none;
        }

        .input-number,
        .input-number-decrement,
        .input-number-increment {
            border: 1px solid #ccc;
            height: 40px;
            user-select: none;
        }

        .input-number-decrement,
        .input-number-increment {
            display: inline-block;
            width: 30px;
            line-height: 38px;
            background: #f1f1f1;
            color: #444;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
        }
        .input-number-decrement:active,
        .input-number-increment:active {
            background: #ddd;
        }

        .input-number-decrement {
            border-right: none;
            border-radius: 4px 0 0 4px;
        }

        .input-number-increment {
            border-left: none;
            border-radius: 0 4px 4px 0;
        }
        div.mini-shopping-cart {
            background: #fff;
            width: 80%;
            max-width: 388px;
            /*margin: 120px auto;*/
            /*padding: 16px 16px 9px 16px;*/
            overflow: hidden;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        div.mini-shopping-cart span {
            -webkit-user-select: all;
            -moz-user-select: all;
            -ms-user-select: all;
            user-select: all;
        }
        div.mini-shopping-cart .mini-cart-item {
            margin-bottom: 16px;
            display: table;
            width: 100%;
            position: relative;
        }
        div.mini-shopping-cart .mini-cart-item div {
            display: table-cell;
            height: 60px;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child {
            width: 60px;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div {
            overflow-x: hidden;
            padding-left: 8px;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div span {
            width: 180px;
            white-space: nowrap;
            display: block;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div span:nth-child(2) {
            color: #aaaaaa;
            font-weight: 100;
            display: inline;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div + div {
            font-weight: 700;
            right: 0;
            padding-right: 8px;
            text-align: right;
            width: 80px;
            position: absolute;
            background: #fff;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div + div .fa-trash-o {
            /*display: none;*/
            margin: 18px -2px 0 0;
            float: right;
            padding: 2px;
            color: #f16d62;
        }
        div.mini-shopping-cart .mini-cart-item div:first-child + div + div .fa-trash-o:hover {
            cursor: pointer;
        }
        div.mini-shopping-cart .mini-cart-item div img {
            width: 60px;
            height: 60px;
            vertical-align: top;
        }
        div.mini-shopping-cart .mini-cart-item:hover .fa-trash-o {
            display: block !important;
        }
        div.mini-shopping-cart .mini-cart-subtotal {
            margin-bottom: 16px;
            display: table;
            width: 100%;
            position: relative;
        }
        div.mini-shopping-cart .mini-cart-subtotal div {
            display: table-cell;
        }
        div.mini-shopping-cart .mini-cart-subtotal div:first-child {
            padding-left: 68px;
        }
        div.mini-shopping-cart .mini-cart-subtotal div:first-child + div {
            font-weight: 700;
            right: 0;
            padding-right: 8px;
            text-align: right;
            width: 80px;
            position: absolute;
            background: #fff;
        }
        div.mini-shopping-cart .mini-cart-subtotal div img {
            width: 60px;
            height: 60px;
            vertical-align: top;
        }
        div.mini-shopping-cart .mini-shopping-cart-buttons {
            text-align: right;
        }

    </style>
    <link rel="stylesheet" href="{{asset('front/vendor/iziToast/iziToast.min.css')}}">
@endsection
@section('content')
    <div class="main-page py-5">

        <div class="container">
            <div class="row">

                <div class="col-lg-8 right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Accueil</a></li>
                            <li class="breadcrumb-item"><a href="{{route('service.list',$service->category->id)}}">{{$service->category->name}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$service->name}}</li>
                        </ol>
                    </nav>

                    <div class="sticky">
                        <ul class="nav nav-tabs">
                            <li>
                                <a class="active" data-toggle="tab" href="#basic">Formulaire</a>
                            </li>
                            @if($service->product)
                                <li>
                                    <a data-toggle="tab" href="#products">Produits</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#standard">Panier</a>
                                </li>
                            @endif
                        </ul>
                        <form autocomplete="off"  method="post" action="{{route('front.order.store',$service->id)}}" class="mb-2" enctype="multipart/form-data">
                            @csrf
                            <div class="tab-content">
                                <div id="basic" class="tab-pane fade show active">
                                    @if(!Auth::user()||$can==0)
                                        <p style="color: red;">
                                            {{!Auth::user() ? 'vous devez être connecté pour remplir la formulaire' : 'vous n\'avez pas le droit d\'utiliser ce service'}}
                                        </p>

                                        <a class="btn button" >Passer la Commande
                                            (<span class="total-price">0.00DT</span>)
                                        </a>
                                    @else
                                        @if($service->besoin)
                                            <div class="form-group">
                                                <h6> <label for="exampleFormControlTextarea1">Décrivez votre besoin</label></h6>
                                                <textarea style="height: 35px;" required name="besoin" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Décrivez votre besoin"></textarea>
                                            </div>
                                        @endif
                                        @if($service->document)
                                            <div class="form-group">
                                                <h6><label for="exampleFormControlTextarea1">Joindre vos Documents</label></h6>
                                                <input multiple name="documents[]" id="input-id" type="file" class="file" data-preview-file-type="text" required>
                                            </div>
                                        @endif
                                        @if($service->date)
                                            <div class="form-row mb-2">
                                                <div class="col">
                                                    <h6><label for="exampleFormControlTextarea1">Choisir la date</label></h6>
                                                    <input readonly="readonly" required name="date" type="text" class="form-control {{$service->multidate==0 ?'thedatepicker' :'thedaterangepicker'}}" placeholder="Cliquez ici pour choisir une date">
                                                </div>
                                            </div>
                                        @endif
                                        <button class="btn button" type="submit">Passer la Commande
                                            (<span class="total-price">0.00DT</span>)
                                        </button>
                                    @endif

                                </div>
                                <div id="standard" class="tab-pane fade">
                                    @if(!Auth::user()||$can==0)
                                        <p style="color: red;">
                                            {{!Auth::user() ? 'vous devez être connecté pour remplir la formulaire' : 'vous n\'avez pas le droit d\'utiliser ce service'}}
                                        </p>

                                        <a class="btn button" >Passer la Commande
                                            (<span class="total-price">0.00DT</span>)
                                        </a>
                                    @else
                                        <div class="mini-shopping-cart">
                                            <div id="no-products" style="padding: 15px">
                                                Aucun Produit sélectionné...
                                            </div>
                                        </div>
                                        <button class="btn button" type="submit">Passer la Commande(<span class="total-price">0.00DT</span>)
                                        </button>
                                    @endif

                                </div>
                                <div id="products" class="tab-pane fade">
                                    @if(!Auth::user()||$can==0)
                                        <p style="color: red;">
                                            {{!Auth::user() ? 'vous devez être connecté pour remplir la formulaire' : 'vous n\'avez pas le droit d\'utiliser ce service'}}
                                        </p>

                                        <a class="btn button" >Passer la Commande
                                            (<span class="total-price">0.00DT</span>)
                                        </a>
                                    @else
                                        @if($service->product==1)
                                            <div class="box shadow-sm rounded bg-white mb-3">
                                                <div class="box-title p-3" style="padding-bottom: 0px!important;">
                                                    <h6 class="m-0 text-muted">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                                                        Liste des Produits
                                                    </h6>
                                                </div>
                                                <div class="box-body p-3">
                                                    <table id="test" class="table nowrap table-hover">
                                                        <thead>
                                                        <tr style="    background: linear-gradient(to right, #4be2aa00, #b9bcbb47);">
                                                            <th>#</th>
                                                            <th>image</th>
                                                            <th>Nom</th>
                                                            <th>prix</th>
                                                            <th>Quantité</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>

                                                        <tbody>
                                                        @if(!Auth::user()||$can==0)
                                                            <tr>
                                                                <p style="color: red;">
                                                                    {{!Auth::user() ? 'vous devez être connecté pour voir la liste des produits' : 'vous n\'avez pas le droit d\'utiliser ce service'}}

                                                                </p>
                                                            </tr>
                                                        @else
                                                            @foreach($products as $product)
                                                                <tr>
                                                                    <td class="product-id">{{$product->id}}</td>
                                                                    <td class="product-img">
                                                                        <img src="{{!$product->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/product/'.$product->id.'/'.$product->image)}}"
                                                                             alt="" class="avatar-lg m-1 rounded-circle ">
                                                                    </td>
                                                                    <td class="product-name">{{$product->name}}</td>
                                                                    <td class="product-price">{{$product->price}}</td>
                                                                    <td class="product-quantity">
                                                                        <div class="input-group mb-3" style="max-width: 120px;">
                                                                            <div class="input-group-prepend">
                                                                                <button class="btn btn-outline-primary js-btn-minus" type="button">−</button>
                                                                            </div>
                                                                            <input type="number" class="form-control text-center quantity-input" value="1" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                                            <div class="input-group-append">
                                                                                <button class="btn btn-outline-primary js-btn-plus" type="button">+</button>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td><a class="btn btn-light btn-sm btn-add-cart">Ajouter</a></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                </div>
                            </div>

                        </form>
                    </div>


                </div>

                <div class="col-lg-4 right">
                    <div class="sticky">
                        <div class="view_slider recommended pt-5">
                            <div class="container">
                                <div class="row">
                                    <div id="aniimated-thumbnials2" style="width: 100%" class="slider-for img-fluid">

                                        @foreach($service->pictures as $picture)
                                            <a href="{{!$picture->name ? asset('assets/images/users/avatar-2.jpg') :asset('storage/pictures/service/'.$service->id.'/images/'.$picture->name)}}">
                                                <img style="width: 100%" src="{{!$picture->name ? asset('assets/images/users/avatar-2.jpg') :asset('storage/pictures/service/'.$service->id.'/images/'.$picture->name)}}" />
                                            </a>
                                        @endforeach

                                    </div>
                                    <div class="inner-slider" style="width: 100%">
                                        <div class="inner-wrapper">
                                            <div class="d-flex align-items-center">
                                                <span class="seller-name">
                                                    <a href="#">{{$service->name}}</a>
                                                    <span class="level hint--top level-one-seller">
                                                        {{$service->category->name}}
                                                    </span>
                                                </span>
                                            </div>
                                            <h3 style="margin-bottom: 15px;">
                                                {!! $service->description !!}
                                            </h3>
{{--                                            <div class="content-info">--}}
{{--                                                <div class="rating-wrapper">--}}
{{--                                                    <span class="gig-rating text-body-2">--}}
{{--                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="15" height="15">--}}
{{--                                                            <path fill="currentColor" d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z">--}}
{{--                                                            </path>--}}
{{--                                                        </svg>--}}
{{--                                                        5.0--}}
{{--                                                        <span>(7)</span>--}}
{{--                                                    </span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class="footer" href="{{route('login')}}">
                                                <div class="price" href="{{route('login')}}">
                                                        @if(!Auth::user())
                                                            <div href="{{route('login')}}" class="rating-wrapper text-center" style="color: #1fd0b6 ;  text-transform: lowercase;">
                                                                Se Connecter pour passer une commande
                                                            </div>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="sticky">--}}
{{--                        <ul class="nav nav-tabs">--}}
{{--                            <li>--}}
{{--                                <a class="active" data-toggle="tab" href="#basic">Formulaire</a>--}}
{{--                            </li>--}}
{{--                            @if($service->product)--}}
{{--                            <li>--}}
{{--                                <a data-toggle="tab" href="#standard">Panier</a>--}}
{{--                            </li>--}}
{{--                            @endif--}}
{{--                                                        <li><a data-toggle="tab" href="product-detail.html#Premium">Premium</a></li>--}}
{{--                        </ul>--}}
{{--                        <form autocomplete="off"  method="post" action="{{route('front.order.store',$service->id)}}" class="mb-2" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="tab-content">--}}
{{--                                <div id="basic" class="tab-pane fade show active">--}}
{{--                                    @if(!Auth::user()||$can==0)--}}
{{--                                            <p style="color: red;">--}}
{{--                                                {{!Auth::user() ? 'vous devez être connecté pour remplir la formulaire' : 'vous n\'avez pas le droit d\'utiliser ce service'}}--}}
{{--                                            </p>--}}

{{--                                        <a class="btn button" >Passer la Commande--}}
{{--                                            (<span class="total-price">0.00DT</span>)--}}
{{--                                        </a>--}}
{{--                                    @else--}}
{{--                                    @if($service->besoin)--}}
{{--                                        <div class="form-group">--}}
{{--                                            <h6> <label for="exampleFormControlTextarea1">Décrivez votre besoin</label></h6>--}}
{{--                                            <textarea style="height: 35px;" required name="besoin" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Décrivez votre besoin"></textarea>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                    @if($service->document)--}}
{{--                                        <div class="form-group">--}}
{{--                                            <h6><label for="exampleFormControlTextarea1">Joindre vos Documents</label></h6>--}}
{{--                                            <input multiple name="documents[]" id="input-id" type="file" class="file" data-preview-file-type="text" required>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                    @if($service->date)--}}
{{--                                        <div class="form-row mb-2">--}}
{{--                                            <div class="col">--}}
{{--                                                <h6><label for="exampleFormControlTextarea1">Choisir la date</label></h6>--}}
{{--                                                <input readonly="readonly" required name="date" type="text" class="form-control {{$service->multidate==0 ?'thedatepicker' :'thedaterangepicker'}}" placeholder="Cliquez ici pour choisir une date">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                    <button class="btn button" type="submit">Passer la Commande--}}
{{--                                        (<span class="total-price">0.00DT</span>)--}}
{{--                                    </button>--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                                <div id="standard" class="tab-pane fade">--}}
{{--                                    @if(!Auth::user()||$can==0)--}}
{{--                                        <p style="color: red;">--}}
{{--                                            {{!Auth::user() ? 'vous devez être connecté pour remplir la formulaire' : 'vous n\'avez pas le droit d\'utiliser ce service'}}--}}
{{--                                        </p>--}}

{{--                                        <a class="btn button" >Passer la Commande--}}
{{--                                            (<span class="total-price">0.00DT</span>)--}}
{{--                                        </a>--}}
{{--                                    @else--}}
{{--                                    <div class="mini-shopping-cart">--}}
{{--                                        <div id="no-products" style="padding: 15px">--}}
{{--                                            Aucun Produit sélectionné...--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <button class="btn button" type="submit">Passer la Commande(<span class="total-price">0.00DT</span>)--}}
{{--                                    </button>--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                                                    <div class="contact-seller-wrapper">--}}
{{--                                                        <a class="fit-button " href="product-detail.html#">Contact Seller</a>--}}
{{--                                                    </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}

                </div>

            </div>
        </div>

        <div class="container mt-5">
            <div class="view_slider recommended">
                <h3>Autres Service</h3>

                <div class="view recommended-slider">
                    @foreach($services as $service)
                    <div>
                        <a href="{{route('service.detail',$service->id)}}">
                            <img class="img-fluid" src="{{!$service->image ? asset('assets/images/users/avatar-2.jpg') :asset('storage/pictures/service/'.$service->id.'/'.$service->image)}}" />
                        </a>
                        <div class="inner-slider">
                            <div class="inner-wrapper">
                                <div class="d-flex align-items-center">
                              <span class="seller-image">
                              <img class="img-fluid"
                                   src="{{!$service->image ? asset('assets/images/users/avatar-2.jpg') :asset('storage/pictures/service/'.$service->id.'/'.$service->image)}}"
                                   alt='' />
                              </span>
                                    <span class="seller-name">
                              <a href="{{route('service.detail',$service->id)}}">{{$service->name}}</a>
                              <span class="level hint--top level-one-seller">
                              {{$service->categories}}
                              </span>
                              </span>
                                </div>
                                <h3>{!! Str::substr($service->description,0,20) !!}...</h3>


                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>


            </div>
        </div>
    </div>
@endsection

@section('js')

{{--        @if($service->multidate==1)--}}
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
{{--        @else--}}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"  ></script>
{{--        @endif--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.fr.min.js"  ></script>

    <script src="{{asset('front/vendor/filer/jquery.filer.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            moment.locale('es');
            // enable fileuploader plugin
            $('#input-id').filer({
                limit: 20,
                maxSize: 50,
                showThumbs:true,
                addMore: true,
                captions:{
                    button: "Joindre Document(s)",
                    feedback: "Choisir les fichiers ",
                }

            });

        });
    </script>
    <script>

        $(document).ready(function () {
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var days = @json($days);
            $('.thedaterangepicker').daterangepicker({
                dateFormat: 'dd-mm-yy',
                language: 'fr',
                opens: 'left',
                minDate: today,
                locale: {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Valider",
                    "cancelLabel": "Annuler",
                    "fromLabel": "Depuis",
                    "toLabel": "A",
                    "customRangeLabel": "Personnalisé",
                    "weekLabel": "S",
                    "daysOfWeek": ["Di","Lu","Ma","Me","Je","Ve","Sa"],
                    "monthNames": ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"],
                    "firstDay": 1
                },
                isInvalidDate: function(date) {
                    return days.includes(date.day()) === true;
                },

            });
            var date = new Date();
            date.setDate(date.getDate()+1);
            $('.thedatepicker').datepicker({
                dateFormat: 'dd-mm-yy',
                language: 'fr',
                startDate: date,
                daysOfWeekDisabled: "{{$service->dateopen}}",
                opens: 'left',
            });



        })
        $(function(){

            var textarea = document.querySelector('textarea');

            textarea.addEventListener('keydown', autosize);

            function autosize(){
                var el = this;
                setTimeout(function(){
                    el.style.cssText = 'height:auto; padding:0';
                    // for box-sizing other than "content-box" use:
                    // el.style.cssText = '-moz-box-sizing:content-box';
                    el.style.cssText = 'height:' + el.scrollHeight + 'px';
                },0);
            }
            var sitePlusMinus = function() {
                $('.js-btn-minus').on('click', function(e){
                    e.preventDefault();
                    if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
                        $(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
                    } else {
                        $(this).closest('.input-group').find('.form-control').val(parseInt(0));
                    }
                });
                $('.js-btn-plus').on('click', function(e){
                    e.preventDefault();
                    $(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
                });
            };
            sitePlusMinus();
        });
    </script>

    <script>
        $(document).ready(function () {
            $('body').on('click', '.btn-add-cart', function () {
                $('.nav-tabs a[href="#standard"]').tab('show');
                if ($('#no-products').length > 0) {
                    $('#no-products').remove();
                }
                let id = parseInt($(this).parent().parent().find('.product-id').text());
                if ($("#item"+id).length > 0) {
                    $("#item"+id).remove();
                }
                let src = $(this).parent().parent().find('.product-img img:first').attr('src');
                let name =  $(this).parent().parent().find('.product-name').text();
                let price = parseFloat($(this).parent().parent().find('.product-price').text()).toFixed(2);
                let quantity = $(this).parent().parent().find('.product-quantity > .input-group > .quantity-input').val();
                let totalprice = quantity * price;
                $('.mini-shopping-cart').append('<div class="mini-cart-item" id="item'+ id +'"> <input type="hidden" name="products['+ id +'][id]" value="'+id+'" /> <input type="hidden" name="products['+ id +'][qty]" value="'+quantity+'" /> <input type="hidden" name="products['+ id +'][price]" value="'+price+'" /> <div><img src="'+ src +'" /></div> <div> <span>'+ name+'<br /></span> <span> DT'+ price+'<br /> Quantité: '+ quantity +'</span> </div> <div> <span class="total-item">$'+ totalprice.toFixed(2) +'<br /></span> <i style="cursor: pointer;" class="fa fa-trash-o remove-product" alt="Remove item"></i> </div> </div>')
                calculateTotal();
            });

            $('body').on('click', '.remove-product',function () {
                $(this).parent().parent().remove();
                if( !$.trim( $('.mini-shopping-cart').html() ).length ) {
                    $('.mini-shopping-cart').html('<div id="no-products" style="padding: 15px"> Aucun Produit sélectionné... </div>')
                }
                calculateTotal();
            })

            function calculateTotal() {
                if ($('.total-item').length > 0) {
                    let sum = 0;
                    $('.total-item').each(function () {
                        let totalitem = parseFloat($(this).text().substring(1));
                        sum+= totalitem;
                    });
                    $('.total-price').text((sum.toFixed(2)+ 'DT' ));
                } else {
                    $('.total-price').text('0.00DT');
                }

            }
        });
    </script>
    <script src="{{asset('front/vendor/iziToast/iziToast.min.js')}}"></script>
    <script>
        @if($errors->all())
        @foreach($errors->all() as $message)
        iziToast.error({
            title: 'Erreur',
            message: '{{ $message }}',
            position: 'bottomRight'
        });
        @endforeach

        @elseif(session()->has('message'))
        iziToast.success({
            title: 'Succès',
            message: '{{ session()->get('message') }}',
            position: 'bottomRight'
        });

        @elseif(session()->has('error'))
        iziToast.error({
            title: 'Erreur',
            message: '{{ session()->get('error') }}',
            position: 'bottomRight'
        });
        @endif
    </script>

@endsection
