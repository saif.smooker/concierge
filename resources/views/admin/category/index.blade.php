@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
        .avatar-upload {
            position: relative;
            max-width: 205px;
            /*margin: 50px auto;*/
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Catégories</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Catégories</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-12" style="margin-bottom: 1%;">
{{--                            <a href="{{route('type.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouveau type</a>--}}
                            <button
                                {{--                                            onclick="document.getElementById('delete').submit()"--}}
                                type="button"
                                class="float-right btn btn-primary"
                                data-toggle="modal"
                                data-target="#modal-new">Créer Une Nouvelle Catégorie
                            </button>
                        </div>
                    </div>



                    <table id="test" class="table nowrap">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Services</th>
                                <th>statut</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    <img src="{{!$category->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/category/'.$category->id.'/'.$category->image)}}"
                                         alt="" class="avatar-sm m-1 rounded-circle">
                                </td>
                                <td>{{$category->name}}</td>
                                <td>{{count($category->services)}}</td>
                                <td>
                                    <h5 class="m-0">
                                    <span class="badge badge-{{$category->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$category->status=='1' ? 'activé' : 'désactivé'}}
                                    </span>
                                    </h5>
                                </td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
{{--                                        <a href="{{route('company.show',$company->id)}}" class="btn btn-success"><i class="uil uil-eye"></i></a>--}}
{{--                                        <a href="{{route('company.edit',$company->id)}}" class="btn btn-info"><i class="uil uil-pen"></i></a>--}}

                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-success"
                                                data-toggle="modal"
                                                data-target="#modal-show{{$category->id}}"><i class="uil uil-eye"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-info"
                                                data-toggle="modal"
                                                data-target="#modal-edit{{$category->id}}"><i class="uil uil-pen"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#modal-error{{$category->id}}"><i class="uil uil-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="modal-show{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Affichage d'une Catégorie</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12">
                                                                    <div>
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                                                                    <div class="col-lg-10">
                                                                                        <input disabled value="{{$category->name}}" type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label">Statut</label>
                                                                                    <div class="col-lg-10">
                                                                                        <select disabled name="status" class="form-control custom-select">
                                                                                            <option {{$category->status==1 ? 'selected' : ''}} value="1">Activé</option>
                                                                                            <option {{$category->status==0 ? 'selected' : ''}} value="0">Désactivé</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                                                                    <div class="col-lg-10">
                                                                                        <div class="avatar-upload">
                                                                                            <div class="avatar-preview">
                                                                                                <div id="imagePreview1" style="background-image: url({{asset('storage/pictures/category/'.$category->id.'/'.$category->image)}});">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <div class="modal fade" id="modal-edit{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Modification d'une Catégorie</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12">
                                                                    <form  method="post" action="{{route('category.update',$category->id)}}" enctype="multipart/form-data">
                                                                        @csrf
                                                                        @method('put')
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                                                                    <div class="col-lg-10">
                                                                                        <input value="{{$category->name}}" type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label">Statut</label>
                                                                                    <div class="col-lg-10">
                                                                                        <select name="status" class="form-control custom-select">
                                                                                            <option {{$category->status==1 ? 'selected' : ''}} value="1">Activé</option>
                                                                                            <option {{$category->status==0 ? 'selected' : ''}} value="0">Désactivé</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                                                                    <div class="col-lg-10">
                                                                                        <input name="image" type='file' accept=".png, .jpg, .jpeg" />

                                                                                        {{--                                                                                        <div class="avatar-upload">--}}
{{--                                                                                            <div class="avatar-edit">--}}
{{--                                                                                                <input name="image" type='file' id="imageUpload{{$category->id}}" accept=".png, .jpg, .jpeg" />--}}
{{--                                                                                                <label for="imageUpload"></label>--}}
{{--                                                                                            </div>--}}
{{--                                                                                            <div class="avatar-preview">--}}
{{--                                                                                                <div id="imagePreview{{$category->id}}" style="background-image: url({{asset('storage/pictures/category/'.$category->id.'/'.$category->image)}});">--}}
{{--                                                                                                </div>--}}
{{--                                                                                            </div>--}}
{{--                                                                                        </div>--}}
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="btn btn-outline-info btn-rounded width-md">Valider</button>
                                                                                <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                                   data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                            </div>
                                                                        </div>

                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <div class="modal fade" id="modal-error{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-trash-alt text-danger display-3"></i>
                                                        <h4 class="text-danger mt-4">Etes vous sûr de vouloir supprimer la catégorie
                                                            <br><b>{{$category->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('category.destroy',$category->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
    <div class="modal fade" id="modal-new" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-errorLabel">Création d'une Catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <i class="uil-plus text-success display-3"></i>
                    <h4 class="text-danger mt-4">Créer une Catégorie</h4>
{{--                    <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>--}}
                    <div class="mt-4">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12">
                                <form  method="post" action="{{route('category.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                                <div class="col-lg-10">
                                                    <input type="file" name="image" class="form-control" id="example-fileinput">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label">Statut</label>
                                                <div class="col-lg-10">
                                                    <select name="status" class="form-control custom-select">
                                                        <option selected value="1">Activé</option>
                                                        <option value="0">Désactivé</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-outline-success btn-rounded width-md">Valider</button>
                                            <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                               data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>

@endsection

@section('script-bottom')
    <!-- Datatables init -->
{{--    <script>--}}
{{--        @foreach($categories as $category)--}}
{{--        function readURL(input) {--}}
{{--            if (input.files && input.files[0]) {--}}
{{--                var reader = new FileReader();--}}
{{--                reader.onload = function(e) {--}}
{{--                    $('#imagePreview{{$category->id}}').css('background-image', 'url('+e.target.result +')');--}}
{{--                    $('#imagePreview{{$category->id}}').hide();--}}
{{--                    $('#imagePreview{{$category->id}}').fadeIn(650);--}}
{{--                }--}}
{{--                reader.readAsDataURL(input.files[0]);--}}
{{--            }--}}
{{--        }--}}
{{--        @endforeach--}}
{{--        $("#imageUpload").change(function() {--}}
{{--            readURL(this);--}}
{{--        });--}}
{{--    </script>--}}
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
