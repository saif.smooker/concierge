@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Catégories</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Categories</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-12" style="margin-bottom: 1%;">
{{--                            <a href="{{route('type.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouveau type</a>--}}
                            <button
                                {{--                                            onclick="document.getElementById('delete').submit()"--}}
                                type="button"
                                class="float-right btn btn-primary"
                                data-toggle="modal"
                                data-target="#modal-new">Créer Une Nouvelle Catégorie
                            </button>
                        </div>
                    </div>



                    <table id="test" class="table nowrap">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Produits</th>
                                <th>statut</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($types as $type)
                            <tr>
                                <td>
                                    <img src="{{!$type->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/type/'.$type->image)}}"
                                         alt="" class="avatar-sm m-1 rounded-circle">
                                </td>
                                <td>{{$type->name}}</td>
                                <td>{{count($type->products)}}</td>
                                <td>
                                    <h5 class="m-0">
                                    <span class="badge badge-{{$type->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$type->status=='1' ? 'activé' : 'désactivé'}}
                                    </span>
                                    </h5>
                                </td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
{{--                                        <a href="{{route('company.show',$company->id)}}" class="btn btn-success"><i class="uil uil-eye"></i></a>--}}
{{--                                        <a href="{{route('company.edit',$company->id)}}" class="btn btn-info"><i class="uil uil-pen"></i></a>--}}

                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-success"
                                                data-toggle="modal"
                                                data-target="#modal-show{{$type->id}}"><i class="uil uil-eye"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-info"
                                                data-toggle="modal"
                                                data-target="#modal-edit{{$type->id}}"><i class="uil uil-pen"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#modal-error{{$type->id}}"><i class="uil uil-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="modal-show{{$type->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Affichage d'une Catégorie</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <img src="{{!$type->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/type/'.$type->image)}}">
{{--                                                        <h4 class="text-danger mt-4">Afficher une Catégorie</h4>--}}
                                                        {{--                    <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>--}}
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12">

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                    <label class=" col-form-label" for="simpleinput">Nom</label>

{{--                                                                                        <input value="{{$type->name}}" type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">--}}
                                                                                        <p>{{$type->name}}</p>

                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                    <label class=" col-form-label">Statut</label>

                                                                                        <h3> <span class="badge badge-{{$type->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                                                                           {{$type->status=='1' ? 'activé' : 'désactivé'}}
                                                                                        </span></h3>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                                   data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                            </div>
                                                                        </div>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <div class="modal fade" id="modal-edit{{$type->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Modification d'une Catégorie</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-pen text-info display-3"></i>
                                                        <h4 class="text-danger mt-4">Modifier une Catégorie</h4>
                                                        {{--                    <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>--}}
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12">
                                                                    <form  method="post" action="{{route('type.update',$type->id)}}" enctype="multipart/form-data">
                                                                        @csrf
                                                                        @method('put')
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                                                                    <div class="col-lg-10">
                                                                                        <input value="{{$type->name}}" type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col">
                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                                                                    <div class="col-lg-10">
                                                                                        <input type="file" name="image" class="form-control" id="example-fileinput">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-lg-2 col-form-label">Statut</label>
                                                                                    <div class="col-lg-10">
                                                                                        <select name="status" class="form-control custom-select">
                                                                                            <option {{$type->status==1 ? 'selected' : ''}} value="1">Activé</option>
                                                                                            <option {{$type->status==0 ? 'selected' : ''}} value="0">Désactivé</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="btn btn-outline-info btn-rounded width-md">Valider</button>
                                                                                <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                                   data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                            </div>
                                                                        </div>

                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <div class="modal fade" id="modal-error{{$type->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-trash-alt text-danger display-3"></i>
                                                        <h4 class="text-danger mt-4">Etes vous sûr de vouloir supprimer la catégorie
                                                            <br><b>{{$type->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('type.destroy',$type->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
    <div class="modal fade" id="modal-new" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-errorLabel">Création d'une Catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <i class="uil-plus text-success display-3"></i>
                    <h4 class="text-danger mt-4">Créer une Catégorie</h4>
{{--                    <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>--}}
                    <div class="mt-4">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12">
                                <form  method="post" action="{{route('type.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la Catégorie">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                                <div class="col-lg-10">
                                                    <input type="file" name="image" class="form-control" id="example-fileinput">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label">Statut</label>
                                                <div class="col-lg-10">
                                                    <select name="status" class="form-control custom-select">
                                                        <option selected value="1">Activé</option>
                                                        <option value="0">Désactivé</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-outline-success btn-rounded width-md">Valider</button>
                                            <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                               data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
{{--    <script>--}}
{{--        $('#test').DataTable();--}}
{{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
