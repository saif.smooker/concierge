@extends('layouts.vertical')
@section('css')
    <!-- Summernote css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/dropify/dropify.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item"><a href="{{route('slider.index')}}">Liste des Sliders</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Création</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Création d'un Slider</h4>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                {{--                <h4 class="header-title mt-0 mb-1">Formulaire de Création</h4>--}}

                <form action="{{route('slider.update',$slider->id)}}" method="post"  class="needs-validation form-horizontal" novalidate enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="row">
                        <div class="col">

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="title">titre</label>
                                <div class="col-lg-9">
                                    <input type="text" name="title" class="form-control" id="title"  placeholder="Titre slider" autocomplete="off" value="{{$slider->title}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="subtitle">sous-titre</label>
                                <div class="col-lg-9">
                                    <input type="text" name="subtitle" class="form-control" id="subtitle"  placeholder="Sous-Titre slider" autocomplete="off" value="{{$slider->subtitle}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="description">description</label>
                                <div class="col-lg-9">
                                    <input type="text" name="description" class="form-control" id="description"  placeholder="Description slider" autocomplete="off" value="{{$slider->description}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="button_text">texte bouton</label>
                                <div class="col-lg-9">
                                    <input type="text" name="button_text" class="form-control" id="button_text"  placeholder="bouton 1 slider" autocomplete="off" value="{{$slider->button_text}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="button_link">lien bouton</label>
                                <div class="col-lg-9">
                                    <input type="text" name="button_link" class="form-control" id="button_link"  placeholder="lien bouton 1 slider" autocomplete="off" value="{{$slider->button_link}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="button_text2">texte bouton 2</label>
                                <div class="col-lg-9">
                                    <input type="text" name="button_text2" class="form-control" id="button_text2"  placeholder="bouton 2 slider" autocomplete="off" value="{{$slider->button_text2}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="button_link2">lien bouton 2</label>
                                <div class="col-lg-9">
                                    <input type="text" name="button_link2" class="form-control" id="button_link2"  placeholder="lien bouton 2 slider" autocomplete="off" value="{{$slider->button_link2}}">
                                </div>
                            </div>

                            <input name="image" type="file" class="dropify" data-max-file-size="3M" data-default-file="{{asset('storage/pictures/slider/'.$slider->image)}}" value="{{$slider->image}}"/>


                        </div>

                    </div>
                    <div class="form-group row">

                        <div class="col-md-12  d-flex justify-content-center text-center">
                            <button  type="submit" class="btn btn-primary btn-lg">Valider</button>
                        </div>

                    </div>
                </form>


            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/dropify/dropify.js') }}"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove':  'Remove',
                'error':   'Ooops, something wrong happended.'
            },
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>default </p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"> replace</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear"> remove </button>',
                errorLine:       '<p class="dropify-error">error </p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });
    </script>

@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection
