@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Sliders</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Sliders</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-12" style="margin-bottom: 1%;">
                            <a href="{{route('slider.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouveau slider</a>
                        </div>
                    </div>



                    <table id="test" class="table nowrap">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Titre</th>
                            <th>Sous-titre</th>
                            <th>Description</th>
                            <th>texte button 1</th>
                            <th>texte button 2</th>
                            <th>Ordre</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')

    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script>
        $(function () {

            var table = $('#test').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('slider.index') }}",
                columns: [
                    {data: 'image', name: 'image'},
                    {data: 'title', name: 'title'},
                    {data: 'subtitle', name: 'subtitle'},
                    {data: 'description', name: 'description'},
                    {data: 'button_text', name: 'button_text'},
                    {data: 'button_text2', name: 'button_text2'},
                    {data: 'order', name: 'order'},
                    {data: 'active', name: 'active'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
    </script>

@endsection

