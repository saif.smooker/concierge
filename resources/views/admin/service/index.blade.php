@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Services</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Services</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-12" style="margin-bottom: 1%;">
                            <a href="{{route('service.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouveau service</a>
                        </div>
                    </div>



                    <table id="test" class="table nowrap">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Catégorie</th>
                                <th>Autorisation</th>
{{--                                <th>Champs utilisé</th>--}}
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>
                                    <img src="{{!$service->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/service/'.$service->id.'/'.$service->image)}}"
                                         alt="" class="avatar-sm m-1 rounded-circle">
                                </td>
                                <td>{{Str::substr($service->name,0,20)}}...</td>
                                <td>{!! Str::substr($service->description,0,20) !!}...</td>
                                <td>{{$service->category->name}}</td>
                                <td>
                                    {{Str::replaceFirst('company','Société', $service->permission) }}
{{--                                    {{Str::replaceFirst('employee', 'Employée', $service->permission)}}--}}
{{--                                    {{Str::replaceFirst('particular', 'Particulier', $service->permission)}}--}}
                                </td>
                                <td>
                                    <h5 class="m-0">
                                    <span class="badge badge-{{$service->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$service->status=='1' ? 'activé' : 'désactivé'}}
                                    </span>
                                    </h5>
                                </td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
                                        <a href="{{route('service.show',$service->id)}}" class="btn btn-success"><i class="uil uil-eye"></i></a>
                                        <a href="{{route('service.pictures',$service->id)}}" class="btn btn-primary"><i class="uil uil-picture"></i></a>
                                        <a href="{{route('service.edit',$service->id)}}" class="btn btn-info"><i class="uil uil-pen"></i></a>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#modal-error{{$service->id}}"><i class="uil uil-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="modal-error{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-trash-alt text-danger display-3"></i>
                                                        <h4 class="text-danger mt-4">Etes vous sûr de vouloir supprimer la société
                                                            <br><b>{{$service->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('service.destroy',$service->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
{{--    <script>--}}
{{--        $('#test').DataTable();--}}
{{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
