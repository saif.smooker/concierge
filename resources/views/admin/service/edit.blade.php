@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
          type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
        .avatar-upload {
            position: relative;
            max-width: 205px;
            /*margin: 50px auto;*/
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-sm-8 col-xl-6">
            <h4 class="mb-1 mt-0">
                Modification du Service " {{$service->name}}"
            </h4>
        </div>
        <div class="col-sm-4 col-xl-6 text-sm-right">

            <div class="btn-group d-none d-sm-inline-block">
                <a href="{{route('service.index')}}" class="btn btn-soft-info btn-sm"><i
                        class="uil uil-enter mr-1"></i>Retour</a>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                {{--                <h4 class="header-title mt-0 mb-1">Formulaire de Création</h4>--}}

                <form action="{{route('service.update',$service->id)}}" method="post"  class="needs-validation form-horizontal" novalidate enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="simpleinput">Nom</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{$service->name}}" name="name" class="form-control" id="simpleinput"  placeholder="Nom de la société">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-email">Besoin</label>
                                <div class="col-lg-9">
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input name="besoin" {{$service->besoin==1 ? 'checked':''}} type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label col-form-label" for="customCheck1" >
                                            Ajouter un champ pour décrire le besoin
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-email">Document</label>
                                <div class="col-lg-9">
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input name="document" {{$service->document==1 ? 'checked':''}} type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label col-form-label" for="customCheck2">
                                            Ajouter un champ pour joindre un document
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-email">Date</label>
                                <div class="col-lg-9">
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input {{$service->date==1 ? 'checked':''}} onclick="checkMe(this.checked);" name="date" type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label col-form-label" for="customCheck3">
                                            Ajouter un champ pour séléctionner une date
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div id="date" style="{{$service->date==1 ? '':'display: none;'}}" class="col-xl-6">
                                <div class="form-group row mt-3 mt-xl-0">
                                    <label>Jours Désactivé</label>
                                    <select name="dateopen[]" class="form-control wide" data-plugin="customselect" multiple>
                                        <option {{Str::contains($service->dateopen,'1') ? 'selected' : ''}} value="1">lundi</option>
                                        <option {{Str::contains($service->dateopen,'2') ? 'selected' : ''}} value="2">mardi</option>
                                        <option {{Str::contains($service->dateopen,'3') ? 'selected' : ''}} value="3" >mercredi</option>
                                        <option {{Str::contains($service->dateopen,'4') ? 'selected' : ''}} value="4">jeudi</option>
                                        <option {{Str::contains($service->dateopen,'5') ? 'selected' : ''}} value="5">vendredi</option>
                                        <option {{Str::contains($service->dateopen,'6') ? 'selected' : ''}} value="6">samedi</option>
                                        <option {{Str::contains($service->dateopen,'0') ? 'selected' : ''}} value="0" >dimanche</option>
                                    </select>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input {{$service->multidate==1 ? 'checked' : ''}} name="multidate" type="checkbox" class="custom-control-input" id="customCheck4">
                                    <label class="custom-control-label col-form-label" for="customCheck4">
                                        Sélection des dates multiple
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-email">Produits</label>
                                <div class="col-lg-9">
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input {{$service->product==1 ? 'checked':''}} onclick="checkMe2(this.checked);" name="product" type="checkbox" class="custom-control-input" id="customCheck5">
                                        <label class="custom-control-label col-form-label" for="customCheck5">
                                            Ajouter une liste des produits
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div id="product" style="{{$service->product==1 ? '':'display: none;'}}" class="col-xl-6">
                                <div class="form-group row mt-3 mt-xl-0">
                                    <label>Catégories des Produits</label>
                                    <select name="type[]" class="form-control wide" data-plugin="customselect" multiple>
                                        @foreach($types as $type)
                                            <option {{$service->types->find($type->id) ? 'selected' : ''}} value="{{$type->id}}" >{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-fileinput">Image</label>
                                <div class="col-lg-9">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input name="image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url({{asset('storage/pictures/service/'.$service->id.'/'.$service->image)}});">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class=" col-lg-3 col-form-label" for="example-placeholder">Categorie</label>
                                <div class="col-lg-9">
                                    <select name="category" class="form-control custom-select">
                                        @foreach($categories as $category)
                                            <option {{$category->id==$service->category->id ? 'selected':''}} value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mt-3 mt-xl-0">
                                <label class=" col-lg-3 col-form-label" for="example-placeholder">Autorisations</label>
                                <div class="col-lg-9">
                                    <select name="permission[]" class="form-control wide" data-plugin="customselect" multiple>
                                        <option {{Str::contains($service->permission,'company') ? 'selected' : ''}} value="company">Société</option>
                                        <option {{Str::contains($service->permission,'employee') ? 'selected' : ''}} value="employee">Employée(collaborateur)</option>
                                        <option {{Str::contains($service->permission,'particular') ? 'selected' : ''}} value="particular" >Particulier</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Statut</label>
                                <div class="col-lg-9">
                                    <select name="status" class="form-control custom-select">
                                        <option {{$service->status==1 ? 'selected' : ''}} value="1">Activé</option>
                                        <option {{$service->status==0 ? 'selected' : ''}} value="0">Désactivé</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Description</label>
                                <textarea name="description" class="summernote">
                                    {{$service->description}}
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-md-12  d-flex justify-content-center text-center">
                            <button  type="submit" class="btn btn-primary btn-lg">Valider</button>
                        </div>

                    </div>
                </form>


            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
        function checkMe(selected)
        {
            if(selected)
            {
                document.getElementById("date").style.display = "";
            }
            else
            {
                document.getElementById("date").style.display = "none";
            }

        }
        function checkMe2(selected)
        {
            if(selected)
            {
                document.getElementById("product").style.display = "";
            }
            else
            {
                document.getElementById("product").style.display = "none";
            }

        }
    </script>
@endsection
