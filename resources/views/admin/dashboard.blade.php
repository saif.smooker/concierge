@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title align-items-center">
        <div class="col-sm-4 col-xl-6">
            <h4 class="mb-1 mt-0">Dashboard</h4>
        </div>
        {{--    <div class="col-sm-8 col-xl-6">--}}
        {{--        <form class="form-inline float-sm-right mt-3 mt-sm-0">--}}
        {{--            <div class="form-group mb-sm-0 mr-2">--}}
        {{--                <input type="text" class="form-control" id="dash-daterange" style="min-width: 190px;" />--}}
        {{--            </div>--}}
        {{--            <div class="btn-group">--}}
        {{--                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"--}}
        {{--                    aria-haspopup="true" aria-expanded="false">--}}
        {{--                    <i class='uil uil-file-alt mr-1'></i>Download--}}
        {{--                    <i class="icon"><span data-feather="chevron-down"></span></i></button>--}}
        {{--                <div class="dropdown-menu dropdown-menu-right">--}}
        {{--                    <a href="#" class="dropdown-item notify-item">--}}
        {{--                        <i data-feather="mail" class="icon-dual icon-xs mr-2"></i>--}}
        {{--                        <span>Email</span>--}}
        {{--                    </a>--}}
        {{--                    <a href="#" class="dropdown-item notify-item">--}}
        {{--                        <i data-feather="printer" class="icon-dual icon-xs mr-2"></i>--}}
        {{--                        <span>Print</span>--}}
        {{--                    </a>--}}
        {{--                    <div class="dropdown-divider"></div>--}}
        {{--                    <a href="#" class="dropdown-item notify-item">--}}
        {{--                        <i data-feather="file" class="icon-dual icon-xs mr-2"></i>--}}
        {{--                        <span>Re-Generate</span>--}}
        {{--                    </a>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </form>--}}
        {{--    </div>--}}
    </div>
@endsection

@section('content')
    @role('admin')
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total
                            Commande</span>
                            <h2 class="mb-0">{{\App\Order::all()->count()}}</h2>
                        </div>
                        <div class="align-self-center">
                            <div id="today-revenue-chart" class="apex-charts"></div>
                            <span class="text-success font-weight-bold font-size-13"><i class='uil uil-arrow-up'></i>
                            10.21%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Sociétes</span>
                            <h2 class="mb-0">{{\App\Company::all()->count()}}</h2>
                        </div>
                        <div class="align-self-center">
                            <div id="today-product-sold-chart" class="apex-charts"></div>
                            <span class="text-danger font-weight-bold font-size-13"><i class='uil uil-arrow-down'></i>
                            5.05%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Particuliers
                            </span>
                            <h2 class="mb-0">{{\App\User::role('particular')->count()}}</h2>
                        </div>
                        <div class="align-self-center">
                            <div id="today-new-customer-chart" class="apex-charts"></div>
                            <span class="text-success font-weight-bold font-size-13"><i class='uil uil-arrow-up'></i>
                            25.16%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Employées</span>
                            <h2 class="mb-0">{{\App\User::role('employee')->count()}}</h2>
                        </div>
                        <div class="align-self-center">
                            <div id="today-new-visitors-chart" class="apex-charts"></div>
                            <span class="text-danger font-weight-bold font-size-13"><i class='uil uil-arrow-down'></i>
                            5.05%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole
    <!-- stats + charts -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <h5 class="card-title header-title border-bottom p-3 mb-0">Overview</h5>
                    <!-- stat 1 -->
                    <div class="media px-3 py-4 border-bottom">
                        <div class="media-body">
                            <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$attente}}</h4>
                            <span class="text-muted">Commandes en Attente</span>
                        </div>
                        <i data-feather="clock" class="align-self-center icon-dual icon-lg"></i>
                    </div>

                    <!-- stat 2 -->
                    <div class="media px-3 py-4 border-bottom">
                        <div class="media-body">
                            <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$encours}}</h4>
                            <span class="text-muted">Commandes en Cours</span>
                        </div>
                        <i data-feather="truck" class="align-self-center icon-dual icon-lg"></i>
                    </div>

                    <!-- stat 3 -->
                    <div class="media px-3 py-4">
                        <div class="media-body">
                            <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$livre}}</h4>
                            <span class="text-muted">Commandes Livré</span>
                        </div>
                        <i data-feather="check-square" class="align-self-center icon-dual icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card">
                <div class="card-body">
                    <a href="" class="btn btn-primary btn-sm float-right">
                        <i class='uil uil-export ml-1'></i> Export
                    </a>
                    <h5 class="card-title mt-0 mb-0 header-title">Recent Orders</h5>

                    <div class="table-responsive mt-4">
                        <table class="table table-hover table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Utilisateur</th>
                                <th scope="col">Service</th>
                                <th scope="col">Date</th>
                                <th scope="col">Prix</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>#{{$order->code}}</td>
                                    <td>{{$order->user->name}}</td>
                                    <td>{{$order->service->name}}</td>
                                    <td>{{$order->created_at->diffForHumans()}}</td>
                                    <td>{{$order->price}}</td>
                                    <td>@if($order->status_id==1)
                                            <span class="badge badge-soft-warning" >En attente</span>
                                        @elseif($order->status_id==2)
                                            <span  class="badge badge-soft-info" >En cours</span>
                                        @else
                                            <span class="badge badge-soft-success" >Livré</span>
                                        @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- row -->


@endsection

@section('script')
    <!-- optional plugins -->
    <script src="{{ URL::asset('assets/libs/moment/moment.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
@endsection

@section('script-bottom')
    <!-- init js -->
    <script src="{{ URL::asset('assets/js/pages/dashboard.init.js') }}"></script>
@endsection
