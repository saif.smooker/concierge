@extends('layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
    <div class="row page-title d-print-none">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Accueil</a></li>
                    <li class="breadcrumb-item"><a href="{{route('order.index')}}">Liste des Commandes</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Commande</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Commande</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Logo & title -->
                    <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-activity-tab" data-toggle="pill" href="#pills-activity"
                               role="tab" aria-controls="pills-activity" aria-selected="true">
                                Commande
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-files-tab" data-toggle="pill" href="#pills-files" role="tab"
                               aria-controls="pills-files" aria-selected="false">
                                Documents & Factures
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-activity" role="tabpanel"
                             aria-labelledby="pills-activity-tab">
                                <div class="clearfix">
                                    <div class="float-sm-right">
                                        <img src="{{asset('assets/images/logo.png')}}" alt="" height="48" />
                                        <h4 class="m-0 d-inline align-middle">Rodina</h4>
                                        <address class="pl-2 mt-2">

                                            <abbr title="Phone">P:</abbr> (123) 456-7890
                                        </address>
                                    </div>
                                    <div class="float-sm-left">
                                        <h4 class="m-0 d-print-none">Commande</h4>
                                        <dl class="row mb-2 mt-3">
                                            <dt class="col-sm-3 font-weight-normal">Numéro Commande :</dt>
                                            <dd class="col-sm-9 font-weight-normal">#{{$order->code}}</dd>

                                            <dt class="col-sm-3 font-weight-normal">Nom Service :</dt>
                                            <dd class="col-sm-9 font-weight-normal">{{$order->service->name}}</dd>

                                            <dt class="col-sm-3 font-weight-normal"> Date Création :</dt>
                                            <dd class="col-sm-9 font-weight-normal">{{$order->parsedate($order->created_at)}}</dd>

                                            <dt class="col-sm-3 font-weight-normal">Date Réservation :</dt>
                                            <dd class="col-sm-9 font-weight-normal">{{$order->parsedate($order->datedeb)}} {{$order->datefin ? 'à'.' '.$order->parsedate($order->datefin) : ''}} </dd>
                                        </dl>
                                    </div>

                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <h6 class="font-weight-normal">Commande Pour:</h6>
                                        <h6 class="font-size-16">{{$order->user->name}}</h6>
                                        <address>
                                            {{Auth::user()->adresse}}
                                            <abbr title="Phone">P:</abbr> (123) 456-7890
                                        </address>
                                    </div> <!-- end col -->

                                    {{--                                        <div class="col-md-6">--}}
                                    {{--                                            <div class="text-md-right">--}}
                                    {{--                                                <h6 class="font-weight-normal">Total</h6>--}}
                                    {{--                                                <h2>{{$order->price}}</h2>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div> <!-- end col -->--}}
                                </div>
                                <!-- end row -->
                                @if($order->product)
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table mt-4 table-centered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nom</th>
                                                        <th style="width: 10%">Prix Unitaire</th>
                                                        <th style="width: 10%">Quantité</th>
                                                        <th style="width: 10%" class="text-right">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($products))
                                                    @foreach($products as $product)
                                                        <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>
                                                                <h5 class="font-size-16 mt-0 mb-2">{{App\Product::find($product->id)->name}}</h5>
                                                                <p class="text-muted mb-0">{{App\Product::find($product->id)->description}}</p>
                                                            </td>
                                                            <td>{{App\Product::find($product->id)->price}}</td>
                                                            <td>{{$product->qty}}</td>
                                                            <td class="text-right">{{$product->price*$product->qty}}</td>
                                                        </tr>
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div> <!-- end table-responsive -->
                                        </div> <!-- end col -->
                                    </div>
                            @endif
                            <!-- end row -->

                                <div class="row">
                                    @if($order->besoin)
                                        <div class="col-sm-6">
                                            <div class="clearfix pt-5">
                                                <h6 class="text-muted">Besoin:</h6>

                                                <small class="text-muted">
                                                    {{$order->besoin}}
                                                </small>
                                            </div>
                                        </div> <!-- end col -->
                                    @endif
                                    <div class="col-sm-6">
                                        <div class="float-right mt-4">
                                            {{--                                                <p><span class="font-weight-medium">total:</span> <span--}}
                                            {{--                                                        class="float-right">{{$total}}</span></p>--}}
                                            {{--                                                <p><span class="font-weight-medium">Discount (10%):</span> <span class="float-right">--}}
                                            {{--                                    &nbsp;&nbsp;&nbsp; $459.75</span></p>--}}
                                            <h3>{{$total}} DT</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->

                                <div class="mt-5 mb-1">
                                    <div class="text-right d-print-none">
                                        <a href="javascript:window.print()" class="btn btn-primary"><i class="uil uil-print mr-1"></i>
                                            Print</a>
                                        <a href="#" class="btn btn-info">Submit</a>
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="pills-files" role="tabpanel" aria-labelledby="pills-files-tab">

                            <h5 class="mt-3">Factures({{$order->bills->count()}})</h5>
                            @foreach($order->bills as $facture)
                                <div class="card mb-2 shadow-none border">
                                    <div class="p-1 px-2">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
                                                <img src="{{asset('assets/images/logo.png')}}" class="avatar-sm rounded" alt="file-image">
                                            </div>
                                            <div class="col pl-0">
                                                <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/facture/'.$facture->name)}}" class="text-muted font-weight-bold">{{$loop->iteration}}</a>
                                                {{--                                                        <p class="mb-0">2.3 MB</p>--}}
                                            </div>
                                            <div class="col-auto">
                                                <!-- Button -->
                                                <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/facture/'.$facture->name)}}" data-toggle="tooltip" data-placement="bottom" title="" class="btn btn-link text-muted btn-lg p-0" data-original-title="Download">
                                                    <i class="uil uil-cloud-download font-size-14"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <h5 class="mt-3">Documents({{$order->documents->count()}})</h5>
                            @foreach($order->documents as $document)
                                {{--                                        <a href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}">Document{{$loop->iteration}}</a>--}}
                                <div class="card mb-2 shadow-none border">
                                    <div class="p-1 px-2">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
                                                <img src="{{asset('assets/images/projects/project-1.jpg')}}" class="avatar-sm rounded" alt="file-image">
                                            </div>
                                            <div class="col pl-0">
                                                <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}" class="text-muted font-weight-bold">{{$document->name}}</a>
                                                {{--                                                        <p class="mb-0">2.3 MB</p>--}}
                                            </div>
                                            <div class="col-auto">
                                                <!-- Button -->
                                                <a target="_blank" href="{{asset('storage/pictures/order/'.$order->id.'/document/'.$document->name)}}" data-toggle="tooltip" data-placement="bottom" title="" class="btn btn-link text-muted btn-lg p-0" data-original-title="Download">
                                                    <i class="uil uil-cloud-download font-size-14"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('script')
@endsection

@section('script-bottom')
@endsection
