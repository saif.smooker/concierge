@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    {{--    <style>--}}
    {{--        label{--}}
    {{--            font-size: 18px;--}}
    {{--        }--}}
    {{--        .avatar-upload {--}}
    {{--            position: relative;--}}
    {{--            max-width: 205px;--}}
    {{--            /*margin: 50px auto;*/--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-edit {--}}
    {{--            position: absolute;--}}
    {{--            right: 12px;--}}
    {{--            z-index: 1;--}}
    {{--            top: 10px;--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-edit input {--}}
    {{--            display: none;--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-edit input + label {--}}
    {{--            display: inline-block;--}}
    {{--            width: 34px;--}}
    {{--            height: 34px;--}}
    {{--            margin-bottom: 0;--}}
    {{--            border-radius: 100%;--}}
    {{--            background: #FFFFFF;--}}
    {{--            border: 1px solid transparent;--}}
    {{--            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);--}}
    {{--            cursor: pointer;--}}
    {{--            font-weight: normal;--}}
    {{--            transition: all 0.2s ease-in-out;--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-edit input + label:hover {--}}
    {{--            background: #f1f1f1;--}}
    {{--            border-color: #d6d6d6;--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-edit input + label:after {--}}
    {{--            content: "\EB0D";--}}
    {{--            font-family: "unicons";--}}
    {{--            color: #757575;--}}
    {{--            position: absolute;--}}
    {{--            top: 5px;--}}
    {{--            left: 0;--}}
    {{--            right: 0;--}}
    {{--            text-align: center;--}}
    {{--            margin: auto;--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-preview {--}}
    {{--            width: 192px;--}}
    {{--            height: 192px;--}}
    {{--            position: relative;--}}
    {{--            border-radius: 100%;--}}
    {{--            border: 6px solid #F8F8F8;--}}
    {{--            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);--}}
    {{--        }--}}
    {{--        .avatar-upload .avatar-preview > div {--}}
    {{--            width: 100%;--}}
    {{--            height: 100%;--}}
    {{--            border-radius: 100%;--}}
    {{--            background-size: cover;--}}
    {{--            background-repeat: no-repeat;--}}
    {{--            background-position: center;--}}
    {{--        }--}}

    {{--    </style>--}}
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Sociétés</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Sociétés</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{--                    <div class="row ">--}}
                    {{--                        <div class="col-md-12" style="margin-bottom: 1%;">--}}
                    {{--                            <a href="{{route('company.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouvelle société</a>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}



                    <table id="test" class="table nowrap table-hover">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Utilisateur</th>
                            <th>Date</th>
                            {{--                            <th>Description</th>--}}
                            <th>Prix</th>
                            <th>facture</th>
                            <th>Statut</th>
                            <th>Voir</th>
                            @unlessrole('company')
                            <th>Modifier</th>
                            @endunlessrole
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($orders as $order)
                            <tr>

                                <td>{{$order->code}}</td>
                                <td>{{$order->user->name}}
                                    @if($order->user->getRoleNames()->first()=="company")
                                        <span class="badge badge-success">société</span>
                                    @elseif($order->user->getRoleNames()->first()=="particular")
                                        <span class="badge badge-info">particulier</span>
                                    @endif
                                </td>
                                <td>{{$order->created_at->format('d/m/Y')}}</td>
                                {{--                                <td>e</td>--}}
                                <td>
                                    @if($order->product)
                                        {{$order->total()}}DT
                                    @endif
                                </td>
                                <td>
                                    @if($order->bills())
                                        <button class="btn btn-light btn-sm" onclick="showModalFacture('{{$order->id}}')">Voir Facture</button>
                                    @endif
                                </td>
                                <td>@if($order->status_id==1)
                                        <span class="badge badge-warning" >En attente</span>
                                    @elseif($order->status_id==2)
                                        <span  class="badge badge-info" >En cours</span>
                                    @else
                                        <span class="badge badge-success" >Livré</span>
                                    @endif</td>
                                <td><a class="btn btn-light btn-sm"  href="{{route('order.show',$order->id)}}">Voir Commande</a></td>
                                @unlessrole('company')
                                <td>
                                    <button type="button" class="btn btn-info" onclick="showModalEdit('{{$order->id}}')">
                                        <i class="uil uil-pen"></i>
                                    </button>
                                </td>
                                @endunlessrole
                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-errorLabel">Mettre a jour les Informations</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="modal-edit-body">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-facture" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-errorLabel">Mettre a jour les Informations</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="modal-facture-body">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    {{--    <script>--}}
    {{--        $('#test').DataTable();--}}
    {{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/fr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>
    <script>
        function showModalEdit(id) {
            $('#modal-edit').modal('show');
            $.ajax({
                url:'{{route('ajax.editform')}}',
                method:'put',
                data: {
                    _token:"{{csrf_token()}}"  ,
                    order_id: id,
                },

                success: function (response) {
                    $('#modal-edit-body').html(response);}
            })}
        function showModalFacture(id){
            $.ajax({
                url:'{{route('ajax.editfacture')}}',
                method:'post',
                data: {
                    _token:"{{csrf_token()}}"  ,
                    order_id: id,
                },
                success: function (response) {
                    $('#modal-facture-body').html(response);
                    var $el1 = $("#" + id);
                    let intial = [];
                    let intialPrev = [];
                    @foreach($factures as $facture)
                    if (id == parseInt('{{$facture->order_id}}')) {
                        intial.push("{{asset('storage/pictures/order/')}}"+"/"+id+"/facture/"+"{{$facture->name}}");
                        intialPrev.push({key:'{{$facture->id}}',url: "{{route('delete.facture')}}" + '/' + '{{$facture->id}}'});
                    }
                    @endforeach
                    $el1.fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        uploadUrl: "{{route('upload.facture')}}" + '/' +id,
                        uploadAsync: true,
                        deleteUrl: "{{route('delete.facture')}}" + '/' +id,
                        showUpload: false, // hide upload button
                        overwriteInitial: false, // append files to initial preview
                        minFileCount: 1,
                        maxFileCount: 5,
                        browseOnZoneClick: true,
                        initialPreviewAsData: true,
                        theme: 'fas',
                        language: "fr",
                        initialPreview: intial,
                        initialPreviewConfig:intialPrev,
                        uploadExtraData: function() {
                            return {
                                _token: "{{ csrf_token() }}",
                            };
                        },
                        deleteExtraData: function() {
                            return {
                                _token: "{{ csrf_token() }}",
                                _method: 'DELETE',
                            };
                        },
                    }).on("filebatchselected", function(event, files) {
                        $("#input-id").fileinput("upload")});
                    $('#modal-facture').modal('show');
                }
            })
        }



    </script>

@endsection
