@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
          type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
        .avatar-upload {
            position: relative;
            max-width: 205px;
            /*margin: 50px auto;*/
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-sm-8 col-xl-6">
            <h4 class="mb-1 mt-0">
                Informations sur "{{$company->name}}"
            </h4>
        </div>
        <div class="col-sm-4 col-xl-6 text-sm-right">
            <div class="btn-group ml-2 d-none d-sm-inline-block">
                <a href="{{route('company.edit',$company->id)}}" class="btn btn-soft-primary btn-sm"><i class="uil uil-edit mr-1"></i>{{__('Modifier')}}</a>
            </div>
            <div class="btn-group d-none d-sm-inline-block">
                <a href="{{route('company.index')}}" class="btn btn-soft-info btn-sm"><i
                        class="uil uil-enter mr-1"></i>Retour</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-0">
                    <h6 class="card-title border-bottom p-3 mb-0 header-title">Informations Générales</h6>
                    <div class="row py-1">
                        <div class="col-xl-4 col-sm-6">
                            <!-- stat 1 -->
                            <div class="media p-3">
                                <i data-feather="grid" class="align-self-center icon-dual icon-lg mr-4"></i>
                                <div class="media-body">
                                    <h4 class="mt-0 mb-0">{{$orderall}}</h4>
                                    <span class="text-muted font-size-13">Commandes</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6">
                            <!-- stat 2 -->
                            <div class="media p-3">
                                <i data-feather="check-square" class="align-self-center icon-dual icon-lg mr-4"></i>
                                <div class="media-body">
                                    <h4 class="mt-0 mb-0">{{$ordersent}}</h4>
                                    <span class="text-muted">Commandes Livré</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6">
                            <!-- stat 3 -->
                            <div class="media p-3">
                                <i data-feather="users" class="align-self-center icon-dual icon-lg mr-4"></i>
                                <div class="media-body">
                                    <h4 class="mt-0 mb-0">{{count($company->users)}}</h4>
                                    <span class="text-muted">Utilisateurs</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- details-->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="container">
                <div class="row">
                    <div class="col-12">
                    <div class="form-group row text-center mb-3 d-flex justify-content-center">
                        <label class="col-lg-3 col-form-label" for="example-fileinput">Image</label>
                        <div class="col-lg-9">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{asset('storage/pictures/company/'.$company->image)}});">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                </div>
        <div class="col">
            <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="simpleinput">Nom</label>
                <div class="col-lg-9">
{{--                    <input type="text" name="name" class="form-control" value="{{$company->name}}" id="simpleinput"  placeholder="Nom de la société">--}}
                    <h4><span class="badge badge-info">{{$company->name}}</span></h4>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="example-email">Email</label>
                <div class="col-lg-9">
                    <h4><span class="badge badge-info">{{$company->admin($company->id)->email}}</span></h4>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="example-placeholder">Adresse</label>
                <div class="col-lg-9">
                    <h4><span class="badge badge-info">{{$company->adresse}}</span></h4>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="example-placeholder">Téléphone</label>
                <div class="col-lg-9">
                    <h4><span class="badge badge-info">{{$company->phone}}</span></h4>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Statut</label>
                <div class="col-lg-9">

                    <h4> <span class="badge badge-{{$company->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$company->status=='1' ? 'activé' : 'désactivé'}}
                        </span></h4>
                </div>
            </div>
        </div>
        <div class="col">


            <div class="form-group row">
                <label class=" col-lg-3 col-form-label" for="example-placeholder">Nombre Employées</label>
                <div class="col-lg-9">
{{--                    <input name="nbr_employee" data-toggle="touchspin" type="text" value="{{$company->nbr_employee}}">--}}
                    <h4><span class="badge badge-info">{{$company->nbr_employee}}</span></h4>
                </div>
            </div>

            <div class="form-group row">
                <label class=" col-lg-3 col-form-label" for="example-placeholder">Prix Par Employée</label>
                <div class="col-lg-9">
{{--                    <input name="prix_employee" data-toggle="touchspin" type="text" value="{{$company->prix_employee}}">--}}
                    <h4><span class="badge badge-info">{{$company->prix_employee}}</span></h4>
                </div>
            </div>

            <div class="form-group row">
                <label class=" col-lg-3 col-form-label" for="example-placeholder">Prix Total</label>
                <div class="col-lg-9">
{{--                    <input disabled data-bts-prefix="DT" data-toggle="touchspin" type="text" value="10">--}}
                    <h4><span class="badge badge-info">{{$company->total($company->prix_employee,$company->nbr_employee)}}</span></h4>
                </div>
            </div>


        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection

@section('script')
@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "uil-eye-slash" );
                    $('#show_hide_password i').removeClass( "uil-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "uil-eye-slash" );
                    $('#show_hide_password i').addClass( "uil-eye" );
                }
            });
        });
    </script>
@endsection
