@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Sociétés</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Sociétés</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-12" style="margin-bottom: 1%;">
                            <a href="{{route('company.create')}}" type="button" class="float-right btn btn-primary">Créer Une nouvelle société</a>
                        </div>
                    </div>



                    <table id="test" class="table nowrap">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Nom</th>
                                <th>Adresse</th>
                                <th>nombre Employées</th>
                                <th>Status</th>
                                <th>Commandes aujourd'hui</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <img src="{{!$user->company->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/company/'.$user->company->image)}}"
                                         alt="" class="avatar-sm m-1 rounded-circle">
                                </td>
                                <td>{{$user->company->name}}</td>
                                <td>{{$user->company->adresse}}</td>
                                <td><a title="voir la liste des employées" href="{{url('/backoffice/employee')}}?company_id={{$user->company->id}}">{{$user->company->users->count()-1}}/{{$user->company->nbr_employee}}<i data-feather="users"></i>({{$user->company->prix_employee}}DT)</a></td>
                                <td>
                                    <h5 class="m-0">
                                    <span class="badge badge-{{$user->company->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$user->company->status=='1' ? 'activé' : 'désactivé'}}
                                    </span>
                                    </h5>
                                </td>

                                <td class="text-center">
                                     <span class=" badge badge-light badge-success ">
                                        {{$order->companyOrders($user->company->id)->whereDate('orders.created_at',$today)->get()->count()}} Commande(s)
                                    </span>
                                </td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
                                        <a href="{{route('company.show',$user->company->id)}}" class="btn btn-success"><i class="uil uil-eye"></i></a>
                                        <a href="{{route('company.edit',$user->company->id)}}" class="btn btn-info"><i class="uil uil-pen"></i></a>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#modal-error{{$user->company->id}}"><i class="uil uil-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="modal-error{{$user->company->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-trash-alt text-danger display-3"></i>
                                                        <h4 class="text-danger mt-4">Etes vous sûr de vouloir supprimer la société
                                                            <br><b>{{$user->company->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('company.destroy',$user->company->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
{{--    <script>--}}
{{--        $('#test').DataTable();--}}
{{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
