@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Payments</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Payments</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="test" class="table nowrap table-hover">
                        <thead>
                            <tr>
{{--                                <th>Image</th>--}}
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Adresse</th>
                                <th>Prix</th>
                                <th>Statut</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>

                        @foreach($payments as $payment)
                            <tr>
                                <td>{{$payment->user->name}}</td>
                                <td>{{$payment->user->email}}</td>
                                <td>{{$payment->user->adresse}}</td>
                                <td>{{$payment->user->price}} DT</td>
                                <td><span class="badge badge-soft-danger">Non Payé</span></td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
                                        <button
                                            {{--                                            onclick="document.getElementById('delete').submit()"--}}
                                            type="button"
                                            class="btn btn-success"
                                            data-toggle="modal"
                                            data-target="#modal-edit{{$payment->id}}"><i class="uil uil-pen"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-info"
                                                data-toggle="modal"
                                                data-target="#modal-mail{{$payment->id}}"><i class="uil-13-plus  uil-fast-mail"></i>
                                        </button>
                                        <button
{{--                                            onclick="document.getElementById('delete').submit()"--}}
                                                type="button"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#modal-mail{{$payment->id}}"><i class="uil-13-plus  uil-exclamation-octagon"></i>
                                        </button>
                                    </div>
                                        <div class="modal fade" id="modal-edit{{$payment->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Confirmation de payement</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-dollar-alt text-success display-3"></i>
                                                        <h4 class="text-success mt-4">Etes vous sûr de vouloir confirmer la payement
                                                            <br><b>{{$payment->user->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('payment.destroy',$payment->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-success btn-rounded width-md" type="submit">confirmer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <div class="modal fade" id="modal-mail{{$payment->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Mail</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-fast-mail-alt text-info display-3"></i>
                                                        <h4 class="text-info mt-4">Etes vous sûr de vouloir envoyer une notification
                                                            <br><b>{{$payment->user->name}}</b></h4>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('payment.destroy',$payment->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
{{--    <script>--}}
{{--        $('#test').DataTable();--}}
{{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
