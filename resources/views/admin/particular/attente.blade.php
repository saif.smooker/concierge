@extends('layouts.vertical')


@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des Particuliers En Attente</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Liste des Particuliers En Attente</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="test" class="table nowrap">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Nom</th>
                            <th>Adresse</th>
                            {{--                                <th>nombre Employées</th>--}}
                            <th>Status</th>
                            <th>Actionss</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <img src="{{!$user->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/user/'.$user->image)}}"
                                         alt="" class="avatar-sm m-1 rounded-circle">
                                </td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->adresse}}</td>
                                {{--                                <td>{{$user->nbr_employee}}<i data-feather="users"></i>({{$user->prix_employee}}DT)</td>--}}
                                <td>
                                    <h5 class="m-0">
                                    <span class="badge badge-{{$user->status=='1' ? 'success' : 'danger'}} badge-pill ">
                                        {{$user->status=='1' ? 'activé' : 'désactivé'}}
                                    </span>
                                    </h5>
                                </td>

                                <td>
                                    <div class="btn-group mb-2 mr-1">
                                        <a href="{{route('particular.accept',$user->id)}}" class="btn btn-success"><i class="uil uil-check"></i></a>
                                        <button
                                            {{--                                            onclick="document.getElementById('delete').submit()"--}}
                                            type="button"
                                            class="btn btn-danger"
                                            data-toggle="modal"
                                            data-target="#modal-error{{$user->id}}"><i class="uil uil-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="modal-error{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-errorLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modal-errorLabel">Suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <i class="uil-trash-alt text-danger display-3"></i>
                                                        <h4 class="text-danger mt-4">Etes vous sûr de vouloir supprimer l'utilisateur
                                                            <br><b>{{$user->name}}</b></h4>
                                                        <p class="w-75 mx-auto text-muted text-center">Cette action est irréversible</p>
                                                        <div class="mt-4">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-3">
                                                                    <form  method="post" action="{{route('particular.destroy',$user->id)}}">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button class="btn btn-outline-danger btn-rounded width-md" type="submit">Supprimer</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="#" class="btn btn-outline-primary btn-rounded width-md"
                                                                       data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    {{--    <script>--}}
    {{--        $('#test').DataTable();--}}
    {{--    </script>--}}
@endsection

@section('script-bottom')
    <!-- Datatables init -->
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
