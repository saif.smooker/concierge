@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
          type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-sm-8 col-xl-6">
            <h4 class="mb-1 mt-0">
                Informations sur "{{$product->name}}"
            </h4>
        </div>
        <div class="col-sm-4 col-xl-6 text-sm-right">
            <div class="btn-group ml-2 d-none d-sm-inline-block">
                <a href="{{route('product.edit',$product->id)}}" class="btn btn-soft-primary btn-sm"><i class="uil uil-edit mr-1"></i>{{__('Modifier')}}</a>
            </div>
            <div class="btn-group d-none d-sm-inline-block">
                <a href="{{route('product.index')}}" class="btn btn-soft-info btn-sm"><i
                        class="uil uil-enter mr-1"></i>Retour</a>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                {{--                <h4 class="header-title mt-0 mb-1">Formulaire de Création</h4>--}}

                <div >

                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                <div class="col-lg-10">
                                    <h4><span class="badge badge-info">{{$product->name}}</span></h4>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="example-email">Description</label>
                                <div class="col-lg-10">
                                    <h4><span class="badge badge-info">{{$product->description}}</span></h4>
                                </div>
                            </div>

                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                <div class="col-lg-10">
                                    <img style="width: 130px;height: 130px;" src="{{asset('storage/pictures/product/'.$product->image)}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="example-fileinput">Catégorie</label>
                                <div class="col-lg-10">
                                    <h4><span class="badge badge-info">{{$product->type->name}}</span></h4>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="example-fileinput">Unité</label>
                                <div class="col-lg-10">
                                    <h4><span class="badge badge-info">{{$product->unit->type}}</span></h4>
                                </div>
                            </div>



                        </div>
                    </div>

                </div>


            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>

@endsection
