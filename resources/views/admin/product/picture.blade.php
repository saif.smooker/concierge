@extends('layouts.vertical')


@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" rel="stylesheet" type="text/css" />


@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">Liste des Produits</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des Images</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Gestion des Images du Produit </h4>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                {{--                <h4 class="header-title mt-0 mb-1">Formulaire de Création</h4>--}}

                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="simpleinput">Images</label>
                                <div class="col-lg-12">
                                    <input id="input-id" name="file" type="file" class="file" data-preview-file-type="text">
                                </div>
                            </div>
                        </div>
                    </div>


            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/fr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script>
        $("#input-id").fileinput({
            allowedFileExtensions: ['jpg', 'png', 'gif'],
            uploadUrl: "{{route('product.pictures.upload',$product->id)}}" ,
            uploadAsync: true,
            language: "fr",
{{--            deleteUrl: "{{route('product.pictures.delete',$product->id)}}",--}}
            showUpload: false, // hide upload button
            overwriteInitial: false, // append files to initial preview
            minFileCount: 1,
            maxFileCount: 5,
            browseOnZoneClick: true,
            initialPreviewAsData: true,
            initialPreviewFileType: 'image',
            theme: 'fas',
            initialPreview: [
                @foreach($pictures as $picture)
                    "{{asset('storage/pictures/product/'.$picture->product_id.'/images/'.$picture->name)}}",
                @endforeach
            ],
            initialPreviewConfig:[
                @foreach($pictures as $picture)
                {key:'{{$picture->id}}',url: "{{route('product.pictures.delete',[$product->id,$picture->id])}}"},
                @endforeach
            ],
            uploadExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                    _method: 'POST',
                };
            },
            deleteExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE',
                };
            },
        }).on('filebeforedelete', function() {
            return new Promise(function(resolve, reject) {
                $.confirm({
                    title: 'Confirmation!',
                    content: 'Êtes-vous sûr de vouloir supprimer ce fichier?',
                    type: 'red',
                    buttons: {
                        ok: {
                            btnClass: 'btn-primary text-white',
                            keys: ['Valider'],
                            action: function(){
                                resolve();
                            }
                        },
                        cancel: function(){
                            $.alert('La suppression du fichier a été abandonnée! ' + krajeeGetCount('file-6'));
                        }
                    }
                });
            });
        }).on('filedeleted', function() {
            setTimeout(function() {
                $.alert('La suppression du fichier a réussi! ' + krajeeGetCount('file-6'));
            }, 900);
        }).on("filebatchselected", function(event, files) {
            $("#input-id").fileinput("upload")});
    </script>
@endsection

@section('script-bottom')

@endsection
