@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
          type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
        .avatar-upload {
            position: relative;
            max-width: 205px;
            /*margin: 50px auto;*/
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-sm-8 col-xl-6">
            <h4 class="mb-1 mt-0">
                Modification du Modérateur " {{$user->name}}"
            </h4>
        </div>
        <div class="col-sm-4 col-xl-6 text-sm-right">

            <div class="btn-group d-none d-sm-inline-block">
                <a href="{{route('worker.index')}}" class="btn btn-soft-info btn-sm"><i
                        class="uil uil-enter mr-1"></i>Retour</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4 mt-0">Formulaire de modification</h5>

                <ul class="nav nav-pills navtab-bg nav-justified">
                    <li class="nav-item">
                        <a href="#information" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="uil-home-alt"></i></span>
                            <span class="d-none d-sm-block">Informations générales</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#password" data-toggle="tab" aria-expanded="true" class="nav-link ">
                            <span class="d-block d-sm-none"><i class="uil-user"></i></span>
                            <span class="d-none d-sm-block">Mot de passe </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content text-muted">
                    <div class="tab-pane show active" id="information">
                        <form action="{{route('worker.update',$user->id)}}" method="post"  class="needs-validation form-horizontal" novalidate enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="simpleinput">Nom</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="name" class="form-control" value="{{$user->name}}" id="simpleinput"  placeholder="Nom de la société">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-email">Email</label>
                                        <div class="col-lg-10">
                                            <input type="email" id="example-email" name="email" value="{{$user->email}}" class="form-control" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Adresse</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="adresse" class="form-control" value="{{$user->adresse}}" placeholder="Adresse" id="example-placeholder">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label" for="example-placeholder">Téléphone</label>
                                        <div class="col-lg-9">
                                            <input type="text" name="phone" class="form-control" value="{{$user->phone}}" id="example-placeholder">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Statut</label>
                                        <div class="col-lg-10">
                                            <select name="status" class="form-control custom-select">
                                                <option {{$user->status==1 ? 'selected' : ''}} value="1">Activé</option>
                                                <option {{$user->status==0 ? 'selected' : ''}} value="0">Désactivé</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-fileinput">Image</label>
                                        <div class="col-lg-10">
                                            <div class="avatar-upload">
                                                <div class="avatar-edit">
                                                    <input name="image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                    <label for="imageUpload"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="imagePreview" style="background-image: url({{asset('storage/pictures/worker/'.$user->image)}});">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="form-group row">--}}
{{--                                        <label class=" col-lg-2 col-form-label" for="example-placeholder">Prix</label>--}}
{{--                                        <div class="col-lg-10">--}}
{{--                                            <input value="{{$user->prix}}" name="prix" data-toggle="touchspin" type="text" value="10">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-md-12  d-flex justify-content-center text-center">
                                    <button  type="submit" class="btn btn-primary btn-lg">Valider</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="tab-pane " id="password">
                        <form method="post" action="{{route('worker.password',$user->id)}}">
                            @csrf
                            @method('put')
                            <div id="show_hide_password " class="form-group row ">
                                <label class="col-lg-2 col-form-label" for="example-password">Password</label>
                                <div class="col-lg-10">
                                    <div id="show_hide_password" class="input-group mb-3">
                                        <input name="password" type="password"  class="form-control" id="example-password" placeholder="Mot de passe">
                                        <div class="input-group-append">
                                            <a class="input-group-text" id="basic-addon2"><i class="uil uil-eye-slash"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div  class="form-group row ">
                                <label class="col-lg-3 col-form-label" for="example-password">Password</label>
                                <div class="col-lg-9">
                                    <div  class="input-group mb-3">
                                        <input name="confirm_password" type="password" class="form-control"  placeholder="Confirmer Mot de passe">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12  d-flex justify-content-center text-center">
                                <button  type="submit" class="btn btn-primary btn-lg">Valider</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "uil-eye-slash" );
                    $('#show_hide_password i').removeClass( "uil-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "uil-eye-slash" );
                    $('#show_hide_password i').addClass( "uil-eye" );
                }
            });
        });
    </script>
@endsection
