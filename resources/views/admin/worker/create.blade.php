@extends('layouts.vertical')


@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css" />
    <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
          type="text/css" />
    <style>
        label{
            font-size: 18px;
        }
        .avatar-upload {
            position: relative;
            max-width: 205px;
            /*margin: 50px auto;*/
        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\EB0D";
            font-family: "unicons";
            color: #757575;
            position: absolute;
            top: 5px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #F8F8F8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@endsection

@section('breadcrumb')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('backoffice')}}">Tableau de Bord</a></li>
                    <li class="breadcrumb-item"><a href="{{route('worker.index')}}">Liste des Modérateurs</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Création</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Création d'un Modérateur</h4>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <form action="{{route('worker.store')}}" method="post"  class="needs-validation form-horizontal" novalidate enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="simpleinput">Nom</label>
                                <div class="col-lg-9">
                                    <input type="text" name="name" class="form-control" id="simpleinput"  placeholder="Nom du Modérateur">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-email">Email</label>
                                <div class="col-lg-9">
                                    <input type="email" id="example-email" name="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div id="show_hide_password " class="form-group row ">
                                <label class="col-lg-3 col-form-label" for="example-password">Password</label>
                                <div class="col-lg-9">
                                    <div id="show_hide_password" class="input-group mb-3">
                                        <input name="password" type="password" class="form-control" id="example-password" placeholder="Mot de passe">
                                        <div class="input-group-append">
                                            <a class="input-group-text" id="basic-addon2"><i class="uil uil-eye-slash"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div  class="form-group row ">
                                <label class="col-lg-3 col-form-label" for="example-password">Password</label>
                                <div class="col-lg-9">
                                    <div  class="input-group mb-3">
                                        <input name="confirm_password" type="password" class="form-control"  placeholder="Confirmer Mot de passe">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-placeholder">Téléphone</label>
                                <div class="col-lg-9">
                                    <input type="text" name="phone" class="form-control"  id="example-placeholder">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label" for="example-fileinput">Image</label>
                                <div class="col-lg-9">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input name="image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url({{asset('images/auth-bg.jpg')}});">
                                            </div>
                                        </div>
                                    </div>                                </div>
                            </div>
{{--                            <div class="form-group row">--}}
{{--                                <label class=" col-lg-3 col-form-label" for="example-placeholder">Prix</label>--}}
{{--                                <div class="col-lg-9">--}}
{{--                                    <input name="prix" data-toggle="touchspin" type="text" value="10">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>

                    </div>
                    <div class="form-group row">

                        <div class="col-md-12  d-flex justify-content-center text-center">
                            <button  type="submit" class="btn btn-primary btn-lg">Valider</button>
                        </div>

                    </div>
                </form>


            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
    <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "uil-eye-slash" );
                    $('#show_hide_password i').removeClass( "uil-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "uil-eye-slash" );
                    $('#show_hide_password i').addClass( "uil-eye" );
                }
            });
        });
    </script>
@endsection
