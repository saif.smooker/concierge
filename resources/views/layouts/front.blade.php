<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gurdeep singh osahan">
    <meta name="author" content="Gurdeep singh osahan">
    <title>Conciergerie</title>
    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('front/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="{{asset('front/vendor/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Material Design Icons -->
    <link href="{{asset('front/vendor/icons/css/materialdesignicons.min.css')}}" media="all" rel="stylesheet" type="text/css">
    <!-- Slick -->
    <link href="{{asset('front/vendor/slick-master/slick/slick.css')}}" rel="stylesheet" type="text/css">
    <!-- Lightgallery -->
    <link href="{{asset('front/vendor/lightgallery-master/dist/css/lightgallery.min.css')}}" rel="stylesheet">
    <!-- Select2 CSS -->
    <link href="{{asset('front/vendor/select2/css/select2-bootstrap.css')}}" />
    <link href="{{asset('front/vendor/select2/css/select2.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->

    <link href="{{asset('front/css/style.css')}}" rel="stylesheet">
    <style>
        #preloader{
            position: fixed;
            top:0;
            bottom: 0;
            right: 0;
            left: 0;
            background: white;
            z-index: 999999;
            display: block;

        }
        .breath{
            margin: 0 auto!important;
            bottom: 50%;
            right: 0;
            position: absolute;
            left: 0;
            animation: transition 1.9s infinite;
        }
        @keyframes transition {
            0%{
                transform: scale(1);
            }
            100%{

                transform: scale(1.2);
            }
        }

    </style>
    @yield('css')
</head>
<body>
<div id="preloader">
    <img class="breath" src="{{asset('assets/images/logo.png')}}">
</div>
<!--    header -->
<nav class="navbar navbar-expand-lg navbar-light topbar static-top shadow-sm bg-white osahan-nav-top px-0">
    <div class="container">
        <!-- Sidebar Toggle (Topbar) -->
        <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
        <!-- Topbar Search -->
        <form class="d-none d-sm-inline-block form-inline mr-auto my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
                <input type="text" class="form-control bg-white small" placeholder="Trouver Un Service..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-success" type="button">
                        <i class="fa fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav align-items-center ml-auto">
            @hasanyrole('company|employee|particular')
            <li class="nav-item dropdown no-arrow no-caret mr-3 dropdown-notifications d-sm-none">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" href="blank.html#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-search fa-fw"></i>
                </a>
                <!-- Dropdown - Messages -->
                <div class="dropdown-menu dropdown-menu-right p-3 shadow-sm animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Find Services..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fa fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li class="nav-item dropdown no-arrow no-caret mr-3 dropdown-notifications">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownAlerts" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
                        <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                    </svg>
                </a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownAlerts">
                    <h6 class="dropdown-header dropdown-notifications-header">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell mr-2">
                            <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                            <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                        </svg>
                        Alerts Center
                    </h6>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <div class="dropdown-notifications-item-icon bg-warning">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity">
                                <polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline>
                            </svg>
                        </div>
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-details">December 29, 2020</div>
                            <div class="dropdown-notifications-item-content-text">This is an alert message. It's nothing serious, but it requires your attention.</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <div class="dropdown-notifications-item-icon bg-info">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart">
                                <line x1="12" y1="20" x2="12" y2="10"></line>
                                <line x1="18" y1="20" x2="18" y2="4"></line>
                                <line x1="6" y1="20" x2="6" y2="16"></line>
                            </svg>
                        </div>
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-details">December 22, 2020</div>
                            <div class="dropdown-notifications-item-content-text">A new monthly report is ready. Click here to view!</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <div class="dropdown-notifications-item-icon bg-danger">
                            <svg class="svg-inline--fa fa-exclamation-triangle fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="exclamation-triangle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                <path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path>
                            </svg>
                            <!-- <i class="fas fa-exclamation-triangle"></i> -->
                        </div>
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-details">December 8, 2020</div>
                            <div class="dropdown-notifications-item-content-text">Critical system failure, systems shutting down.</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <div class="dropdown-notifications-item-icon bg-success">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus">
                                <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="8.5" cy="7" r="4"></circle>
                                <line x1="20" y1="8" x2="20" y2="14"></line>
                                <line x1="23" y1="11" x2="17" y2="11"></line>
                            </svg>
                        </div>
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-details">December 2, 2020</div>
                            <div class="dropdown-notifications-item-content-text">New user request. Woody has requested access to the organization.</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-footer" href="alerts.html">View All Alerts</a>
                </div>
            </li>
            <li class="nav-item dropdown no-arrow no-caret mr-3 dropdown-notifications">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownMessages" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail">
                        <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                        <polyline points="22,6 12,13 2,6"></polyline>
                    </svg>
                </a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownMessages">
                    <h6 class="dropdown-header dropdown-notifications-header">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail mr-2">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                        Message Center
                    </h6>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <img class="dropdown-notifications-item-img" src="images/user/s7.png">
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                            <div class="dropdown-notifications-item-content-details">Emily Fowler · 58m</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-item" href="blank.html#!">
                        <img class="dropdown-notifications-item-img" src="images/user/s8.png">
                        <div class="dropdown-notifications-item-content">
                            <div class="dropdown-notifications-item-content-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                            <div class="dropdown-notifications-item-content-details">Diane Chambers · 2d</div>
                        </div>
                    </a>
                    <a class="dropdown-item dropdown-notifications-footer" href="messages.html">Read All Messages</a>
                </div>
            </li>
            <li class="nav-item dropdown no-arrow no-caret dropdown-user">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="img-fluid" src="{{!Auth()->user()->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/user/'.Auth()->user()->image)}}">
                </a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                    <h6 class="dropdown-header d-flex align-items-center">
                        <img class="dropdown-user-img" src="{{!Auth()->user()->image ? asset('assets/images/users/avatar-2.jpg') : asset('storage/pictures/user/'.Auth()->user()->image)}}">
                        <div class="dropdown-user-details">
                            <div class="dropdown-user-details-name">{{auth()->user()->name}}</div>
                            <div class="dropdown-user-details-email">{{auth()->user()->email}}</div>
                        </div>
                    </h6>
                    @hasrole('company')
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('backoffice')}}">
                        <div class="dropdown-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings">
                                <circle cx="12" cy="12" r="3"></circle>
                                <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                            </svg>
                        </div>
                        Backoffice
                    </a>
                    @endhasrole
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('compte.edit')}}">
                        <div class="dropdown-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings">
                                <circle cx="12" cy="12" r="3"></circle>
                                <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                            </svg>
                        </div>
                        Mon Profil
                    </a>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="{{route('compte')}}">
                        <div class="dropdown-item-icon">
                                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
                        </div>
                        Mes Commandes
                    </a>
                    <a class="dropdown-item" href="{{route('home')}}/tickets">
                        <div class="dropdown-item-icon">
                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>
                        </div>
                        Mes Réclamations
                    </a>
                    <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                        <div class="dropdown-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>
                        </div>
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
                @else
                        @unlessrole('admin|worker')
                        <li class="" >
                            <a href="{{route('login')}}" class="btn btn-success">Se Connecter</a>
                            <a href="{{route('register')}}" class="btn btn-success">S'inscrire</a>
                        </li>
                @else
                    <a class="c-btn c-fill-color-btn" href="{{route('backoffice')}}">
                        <i class="fa fa-fw fa-arrow-circle-o-left"></i>
                        <span>Backoffice</span></a>
                        @endunlessrole
            @endhasanyrole
        </ul>
    </div>
</nav>
<!-- Navigation -->
<div class="navbar navbar-expand-lg navbar-light bg-white osahan-nav-mid px-0 border-top shadow-sm">
    <div class="container">
        <ul class="row navbar-nav">
            <li class="navbar-nav">
                <a class="nav-link " href="{{route('home')}}" >
                    Accueil
                </a>
            </li>
            <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  Comment ca Marche ? </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item " href="#">Conciergerie d'Entreprise</a></li>
                        <li><a class="dropdown-item " href="#">Conciergerie de Professions libérales</a></li>
                    </ul>
            </li>
            <li class="navbar-nav">
                <a class="nav-link" href="{{route('about')}}">A propos</a>
            </li>
            <li class=" navbar-nav">
                <a class="nav-link" href="{{route('contact')}}">Contact</a>
            </li>

        </ul>
    </div>
</div>
<div class="navbar navbar-expand-lg navbar-light bg-white osahan-nav-mid px-0 border-top shadow-sm">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                @foreach($cat as $category)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{route('service.list',$category->id)}}" data-toggle="dropdown">  {{$category->name}}  </a>
                    <ul class="dropdown-menu">

                        @foreach($category->authServices() as $service)
                            <li>
                                <a class="dropdown-item " href="{{route('service.detail',$service->id)}}">{{$service->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                @endforeach

            </ul>
        </div>
{{--        @hasrole('company')--}}
{{--        <ul class="navbar-nav ml-auto">--}}
{{--            <li class="nav-item">--}}
{{--                <a class="btn c-fill-color-btn" href="{{route('backoffice')}}">--}}
{{--                    <i class="fa fa-fw fa-arrow-circle-o-left"></i>--}}
{{--                    <span>Backoffice</span></a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--        @endhasrole--}}
    </div>
</div>

    @yield('content')
<footer class="bg-white">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="footer-list">
                <h2><img style="width: 80%" src="{{asset('assets/images/logo.png')}}"></h2>
                <div class="copyright" style="margin-left: 0px!important;">
                    <ul class="social">
                        <li>
                            <a href="blank.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="blank.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="blank.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="blank.html#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="blank.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-list">
                <h2>Nos services</h2>
                <ul class="list">
                    @foreach($cat as $category)
                    <li><a href="{{route('service.list',$category->id)}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="footer-list">
                <h2>A propos</h2>
                <ul class="list">
                    <li><a href="{{route('about')}}">Qui sommes-nous ?</a></li>
                </ul>
            </div>
            <div class="footer-list">
                <h2>Liens utiles</h2>
                <ul class="list">
                    <li><a href="{{route('contact')}}" >Nous contacter</a></li>
                </ul>
            </div>
{{--            <div class="footer-list">--}}
{{--                <h2>Community</h2>--}}
{{--                <ul class="list">--}}
{{--                    <li><a href="blank.html#">Events</a></li>--}}
{{--                    <li><a href="blank.html#">Blog</a></li>--}}
{{--                    <li><a href="blank.html#">Forum</a></li>--}}
{{--                    <li><a href="blank.html#">Community Standards</a></li>--}}
{{--                    <li><a href="blank.html#">Podcast</a></li>--}}
{{--                    <li><a href="blank.html#">Affiliates</a></li>--}}
{{--                    <li><a href="blank.html#">Invite a Friend</a></li>--}}
{{--                    <li><a href="blank.html#">Become a Seller</a></li>--}}
{{--                    <li><a href="blank.html#" >Miver--}}
{{--                            Elevate<small>Exclusive Benefits</small></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <div class="footer-list">--}}
{{--                <h2>More From Miver</h2>--}}
{{--                <ul class="list">--}}
{{--                    <li><a href="blank.html#">Miver--}}
{{--                            Pro</a>--}}
{{--                    </li>--}}
{{--                    <li><a href="blank.html#">Miver--}}
{{--                            Studios</a>--}}
{{--                    </li>--}}
{{--                    <li><a href="blank.html#">Miver--}}
{{--                            Logo Maker</a>--}}
{{--                    </li>--}}
{{--                    <li><a href="blank.html#">Get Inspired</a></li>--}}
{{--                    <li><a href="blank.html#">ClearVoice<small>Content Marketing</small></a></li>--}}
{{--                    <li><a href="blank.html#">AND CO<small>Invoice Software</small></a></li>--}}
{{--                    <li><a href="blank.html#">Learn<small>Online Courses</small></a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
        </div>
        <div class="copyright">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('assets/images/logo.png')}}">
                </a>
            </div>
            <p>© Copyright 2020 Rodina. All Rights Reserved
            </p>
            <ul class="social">
                <li>
                    <a href="blank.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="blank.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="blank.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="blank.html#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="blank.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript -->
<script src="{{asset('front/vendor/jquery/jquery.min.js')}}"></script>
<script>
    $(window).on("load",function () {
        $('#preloader').hide();
    });
</script>
<script src="{{asset('front/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Contact form JavaScript -->
{{--<!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->--}}
{{--<script src="{{asset('front/js/jqBootstrapValidation.htm')}}"></script>--}}
{{--<script src="{{asset('front/js/contact_me.htm')}}"></script>--}}
<!-- Slick -->
<script src="{{asset('front/vendor/slick-master/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<!-- lightgallery -->
<script src="{{asset('front/vendor/lightgallery-master/dist/js/lightgallery-all.min.js')}}"></script>
<!-- select2 Js -->
<script src="{{asset('front/vendor/select2/js/select2.min.js')}}"></script>
<!-- Custom -->
@yield('js')
<script src="{{asset('front/js/custom.js')}}"></script>


</body>
</html>
