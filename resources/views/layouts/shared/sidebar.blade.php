<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="media user-profile mt-2 mb-2">
        <img src="{{asset('storage/pictures/user/'.Auth::user()->image)}}" class="avatar-sm rounded-circle mr-2" alt="Shreyu" />
{{--        <img src="{{ URL::asset('assets//images/users/avatar-7.jpg') }}" class="avatar-xs rounded-circle mr-2" alt="Shreyu" />--}}

        <div class="media-body">
            <h6 class="pro-user-name mt-0 mb-0">{{Auth::user()->name}}</h6>
            <span class="pro-user-desc">
                @role('admin') Admin @endrole
                 @role('worker') Modérateur @endrole
                @role('company') Société @endrole
            </span>
        </div>
        <div class="dropdown align-self-center profile-dropdown-menu">
            <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                aria-expanded="false">
                <span data-feather="chevron-down"></span>
            </a>
            <div class="dropdown-menu profile-dropdown">
                @role('admin')
                <a href="{{route('user.edit',Auth::user()->id)}}" class="dropdown-item notify-item">
                    <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                    <span>Mon compte</span>
                </a>
                @endrole
{{--                <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                    <i data-feather="settings" class="icon-dual icon-xs mr-2"></i>--}}
{{--                    <span>Settings</span>--}}
{{--                </a>--}}

                <div class="dropdown-divider"></div>

                <a href="{{route('logout')}}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                    <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                    <span>Déconnexion</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
    <div class="sidebar-content">
        <!--- Sidemenu -->
        <div id="sidebar-menu" class="slimscroll-menu">
            @include('layouts.shared.app-menu')
        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
