<ul class="metismenu" id="menu-bar">
    <li class="menu-title">Navigation</li>

    <li>
        <a href="{{route('backoffice')}}">
            <i data-feather="home"></i>
{{--            <span class="badge badge-success float-right">1</span>--}}
            <span> Dashboard </span>
        </a>
    </li>
    <li class="menu-title">Navigation</li>
    @hasrole('admin|worker')
    <li>
        <a href="javascript: void(0);">
            <i data-feather="briefcase"></i>
            <span> Sociétés </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">

            <li>
                <a href="{{route('company.index')}}">Liste Des Société</a>
            </li>
            <li>
                <a href="{{route('company.create')}}">Ajouter une nouvelle Société</a>
            </li>
            <li>
                <a href="{{route('company.attente')}}">Sociétés En attente</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="user"></i>
            <span> Particulier </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('particular.index')}}">Liste Des Particuliers</a>
            </li>
            <li>
                <a href="{{route('particular.create')}}">Ajouter un Utilisateur Particulier</a>
            </li>
            <li>
                <a href="{{route('particular.attente')}}">Particulier En attente</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="users"></i>
            <span> Modérateurs </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('worker.index')}}">Liste Des Modérateurs</a>
            </li>
            <li>
                <a href="{{route('worker.create')}}">Ajouter un nouveau Modérateur</a>
            </li>
        </ul>
    </li>
    @endhasrole
    @role('admin|worker|company')
    <li>
        <a href="javascript: void(0);">
            <i data-feather="users"></i>
            <span> Employées </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('employee.index')}}">Liste Des Employées</a>
            </li>
            @if(Auth::user()->canadd())
            <li>
                <a href="{{route('employee.create')}}">Ajouter un nouveau Employée</a>
            </li>
                @endif
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="shopping-bag"></i>
            <span> Commandes </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('order.index')}}">Liste Des Commande</a>
            </li>
            <li>
                <a href="{{route('order.attente')}}">Commandes En Attente</a>
            </li>
            <li>
                <a href="{{route('order.cours')}}">Commandes En Cours</a>
            </li>
            <li>
                <a href="{{route('order.livre')}}">Commandes Livré</a>
            </li>
        </ul>
    </li>
    @endrole
    @role('admin|worker')
{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="shopping-bag"></i>--}}
{{--            <span> Commandes </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}

{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="{{route('order.index')}}">Liste Des Commandes</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{route('worker.create')}}">Ajouter un nouveau Commandes</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
    <li>
        <a href="javascript: void(0);">
            <i data-feather="truck"></i>
            <span> Services </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('service.index')}}">Liste Des Services</a>
            </li>
            <li>
                <a href="{{route('service.create')}}">Ajouter un nouveau Service</a>
            </li>
            <li>
                <a href="{{route('category.index')}}">Gestion des Catégories</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="shopping-cart"></i>
            <span> Produits </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('product.index')}}">Liste Des Produits</a>
            </li>
            <li>
                <a href="{{route('product.create')}}">Ajouter un nouveau Produit</a>
            </li>
            <li>
                <a href="{{route('type.index')}}">Gestion des Catégories</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript: void(0);">
            <i data-feather="dollar-sign"></i>
            <span> Paiements </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('payment.index')}}">Statut des Payments </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{route('home')}}/tickets">
            <i data-feather="meh"></i>
            <span> Réclamations </span>
        </a>

    </li>
    @endhasrole
    @hasrole('admin')
    <li>
        <a href="javascript: void(0);">
            <i data-feather="settings"></i>
            <span> Paramétres Génrales </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="{{route('slider.index')}}">Sliders</a>
            </li>
        </ul>
    </li>
    @endhasrole
{{--    <li class="menu-title">Apps</li>--}}
{{--    <li>--}}
{{--        <a href="/apps/calendar">--}}
{{--            <i data-feather="calendar"></i>--}}
{{--            <span> Calendar </span>--}}
{{--        </a>--}}
{{--    </li>--}}
{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="inbox"></i>--}}
{{--            <span> Email </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}

{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/apps/email/inbox">Inbox</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/apps/email/read">Read</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/apps/email/compose">Compose</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="briefcase"></i>--}}
{{--            <span> Projects </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}

{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/apps/project/list">List</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/apps/project/detail">Detail</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="bookmark"></i>--}}
{{--            <span> Tasks </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}

{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/apps/task/list">List</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/apps/task/board">Kanban Board</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    <li class="menu-title">Custom</li>--}}
{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="file-text"></i>--}}
{{--            <span> Pages </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}
{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/pages/starter">Starter</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/pages/profile">Profile</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/pages/activity">Activity</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/pages/invoice">Invoice</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/pages/pricing">Pricing</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/pages/maintenance">Maintenance</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/errors/404">Error 404</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/errors/500">Error 500</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}

{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="layout"></i>--}}
{{--            <span> Layouts </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}
{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/layout-example/horizontal">Horizontal Nav</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/rtl">RTL</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example//dark">Dark</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/scrollable">Scrollable</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/boxed">Boxed</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/loader">With Pre-loader</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/dark-sidebar">Dark Side Nav</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/layout-example/condensed-sidebar">Condensed Nav</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}

{{--    <li class="menu-title">Components</li>--}}

{{--    <li>--}}
{{--        <a href="javascript: void(0);">--}}
{{--            <i data-feather="package"></i>--}}
{{--            <span> UI Elements </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}
{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/ui/bootstrap">Bootstrap UI</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="javascript: void(0);" aria-expanded="false">Icons--}}
{{--                    <span class="menu-arrow"></span>--}}
{{--                </a>--}}
{{--                <ul class="nav-third-level" aria-expanded="false">--}}
{{--                    <li>--}}
{{--                        <a href="/ui/icons-feather">Feather Icons</a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="/ui/icons-unicons">Unicons Icons</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/ui/widgets">Widgets</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}

{{--    <li>--}}
{{--        <a href="javascript: void(0);" aria-expanded="false">--}}
{{--            <i data-feather="file-text"></i>--}}
{{--            <span> Forms </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}
{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/forms/basic">Basic Elements</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/forms/advanced">Advanced</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/forms/validation">Validation</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/forms/wizard">Wizard</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/forms/editor">Editor</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/forms/fileupload">File Uploads</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}

{{--    <li>--}}
{{--        <a href="/charts" aria-expanded="false">--}}
{{--            <i data-feather="pie-chart"></i>--}}
{{--            <span> Charts </span>--}}
{{--        </a>--}}
{{--    </li>--}}

{{--    <li>--}}
{{--        <a href="javascript: void(0);" aria-expanded="false">--}}
{{--            <i data-feather="grid"></i>--}}
{{--            <span> Tables </span>--}}
{{--            <span class="menu-arrow"></span>--}}
{{--        </a>--}}
{{--        <ul class="nav-second-level" aria-expanded="false">--}}
{{--            <li>--}}
{{--                <a href="/table/basic">Basic</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/table/datatables">Advanced</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
</ul>
