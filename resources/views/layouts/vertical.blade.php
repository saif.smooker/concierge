<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>Concierge - Admin Backoffice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- iziToast alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('front/vendor/iziToast/iziToast.min.css')}}">

        @include('layouts.shared.head')


</head>



<body>



{{--<!-- Pre-loader -->--}}
{{--<div id="preloader">--}}
{{--    <div id="status">--}}
{{--        <div class="spinner">--}}
{{--            <div class="circle1"></div>--}}
{{--            <div class="circle2"></div>--}}
{{--            <div class="circle3"></div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- End Preloader-->--}}


    <div id="wrapper">

        @include('layouts.shared.header')
        @include('layouts.shared.sidebar')

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">
                    @yield('breadcrumb')
                    @yield('content')
                </div>
            </div>

            @include('layouts.shared.footer')
        </div>
    </div>

    @include('layouts.shared.rightbar')

    @include('layouts.shared.footer-script')

    @if (getenv('APP_ENV') === 'local')
    <script id="__bs_script__">//<![CDATA[
        document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
    //]]></script>
    @endif
<!-- iziToast alert -->
<script src="{{asset('front/vendor/iziToast/iziToast.min.js')}}" type="text/javascript"></script>
<script>
    @if($errors->all())
    @foreach($errors->all() as $message)
    iziToast.error({
        title: 'Erreur',
        message: '{{ $message }}',
        position: 'bottomRight'
    });
    @endforeach

    @elseif(session()->has('message'))
    iziToast.success({
        title: 'Succès',
        message: '{{ session()->get('message') }}',
        position: 'bottomRight'
    });

    @elseif(session()->has('error'))
    iziToast.error({
        title: 'Erreur',
        message: '{{ session()->get('error') }}',
        position: 'bottomRight'
    });
    @endif
</script>
</body>

</html>
