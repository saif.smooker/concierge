<form  method="post" action="{{route('order.status',$order->id)}}" >
    @csrf
    @method('put')
    <div class="row">
        <div class="col">
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Statut</label>
                <div class="col-lg-10">
                    <select name="status" class="form-control custom-select">
                        <option {{$order->status_id==1 ? 'selected' : ''}} class="badge badge-warning" value="1">En attente</option>
                        <option {{$order->status_id==2 ? 'selected' : ''}} class="badge badge-info" value="2">En cours</option>
                        <option {{$order->status_id==3 ? 'selected' : ''}} class="badge badge-success" value="3">Livré</option>
                    </select>
                </div>
            </div>
            @if(!$order->product)
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Prix</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="price">
                    </div>
                </div>
            @endif




        </div>

        <div class="col-md-12">
            <button type="submit" class="btn btn-outline-info btn-rounded width-md">Valider</button>
            <a href="#" class="btn btn-outline-primary btn-rounded width-md"
               data-dismiss="modal"><i class="uil uil-arrow-left mr-1"></i> Fermer</a>
        </div>
    </div>

</form>
