<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $guarded=[];

    public function order(){
        return $this->belongsTo('App\Order');
    }
    public function contain($id)
    {
        $facture=Bill::where('order_id',$id)->get();
        if ($facture)
        return true;
        else
            return false;
    }
}
