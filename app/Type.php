<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded=[];

    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function services()
    {
        return $this->belongsToMany('App\Service','service_type');
    }
}
