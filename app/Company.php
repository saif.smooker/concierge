<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded=[];

    public function users()
    {
        return $this->hasMany('App\User');
    }
    public function admin($id)
    {
        return User::role('company')->where('company_id',$id)->first();
    }
    public function total($a,$b)
    {
        return $a*$b;
    }
    public function canAdd()
    {
        if(($this->users()->count())<=($this->nbr_employee))
            return true;
        else
            return false;
    }
}
