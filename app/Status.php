<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $guarded=[];
    public function order()
    {
        return $this->belongsToMany('App\Order');
    }
}
