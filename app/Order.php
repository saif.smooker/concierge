<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded=[];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function service()
    {
        return $this->belongsTo('App\Service');
    }
    public function documents(){
        return $this->hasMany('App\Document');
    }
    public function bills(){
        return $this->hasMany('App\Bill');
    }
    public function companyOrders($companyid)
    {
        return Order::join('users','users.id','orders.user_id')
            ->select('orders.*')
            ->where('users.company_id',$companyid);

    }
    public function total()
    {
        if ($this->product)
        {
            $orders=json_decode($this->product);
            $total=0;
            foreach ((array)$orders as $order)
            {
                $total+=$order->price*$order->qty;
            }
            return $total;
        }

    }

    function parsedate($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

}
