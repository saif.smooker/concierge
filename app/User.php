<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function canadd()
    {
        if ($role=Auth::user()->getRoleNames()->first()=="company")
        {
            $company=Company::where('id',Auth::user()->company_id)->first();

            if(($company->users->count())==((int)$company->nbr_employee+1))
                return false;
            else
                return true;
        }
        else
            return true;
    }
    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }
    protected function credentials(Request $request)
    {
        return ['email' => $request->{$this->name()}, 'password' => $request->password, 'status' => 1];
    }
//    public function store(EmployeRequest $request)
//    {
//        $plainpass = Str::random(8);
//        $params = ['role' => 'ROLE_EMPLOYE', 'password' => $plainpass];
//        if ($image = $request->files->get('image')) {
//            $destinationPath = 'images/employes/'; // upload path
//            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
//            $image->move($destinationPath, $profileImage);
//            $params['image'] = $profileImage;
//        }
//        $employe = User::create(array_merge($request->except(['privileges']), $params));
//        $employe->privileges()->attach($request->get('privileges'));
//        Mail::to($employe->email)->send(new WelcomeMailGs($employe,$plainpass));
//        return redirect()->route('employe.index')->with('success','Employe Ajouté');
//    }
}
