<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $guarded=[];

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function authServices()
    {
        if (!Auth::check())
            return $this->services()->get();
        else{
//            $user=User::find(Auth::user()->id);
            $user=Auth::user();
            $role=$user->getRoleNames()->first();
            return $this->services()->where('permission','LIKE','%'.$role.'%')->get();
        }


    }

}
