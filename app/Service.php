<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function types()
    {
        return $this->belongsToMany('App\Type','service_type');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function pictures()
    {
        return $this->hasMany('App\ServicePicture');
    }
}
