<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ParticularController extends Controller
{

    public function index()
    {
        $users = User::role('particular')->where('en_attente', 0)->get();
        return view('admin.particular.index',compact('users'));
    }

    public function attente() {
        $users = User::role('particular')->where('en_attente', 1)->get();
        return view('admin.particular.attente',compact('users'));
    }

    public function accept($user_id) {
        $user = User::find($user_id);
        $user->status = 1;
        $user->en_attente = 0;
        $user->save();

        return redirect()->back()->with('message', 'Utilisateur Particulier Accepté');
    }

    public function create()
    {
        return view('admin.particular.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
            'adresse' => 'required',
            'prix' => 'required',
            'status' => 'required',
        ]);
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=$request->password;
        $user->adresse=$request->adresse;
        $user->prix=$request->prix;
        $user->status=$request->status;
        $user->date=date('Y:m:d');
        $user->assignRole('particular');
        $user->save();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user',$imageName);
            $user->image=$imageName;
            $user->save();
        }
        return redirect(route('particular.index'))->with('message','l\'utilisateur particulier '.$user->name.' a été crée avec succès' );
    }


    public function show($id)
    {
        $user = User::find($id);
        return view('admin.particular.show',compact('user'));
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.particular.edit',compact('user'));
    }

    public function password(Request $request, $id)
    {
        $request->validate([
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);
        $user = User::find($id);
        $user->password=$request->password;
        $user->update();
        return redirect(route('particular.index'))->with('message','le mot de pass de l\'utilisateur particulier '.$user->name.' a été modifiée avec succès' );
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'adresse' => 'required',
            'prix' => 'required',
            'status' => 'required',
        ]);
        $user = User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->prix=$request->prix;
        $user->adresse=$request->adresse;
        $user->status=$request->status;
        $user->update();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user',$imageName);
            $user->image=$imageName;
            $user->update();
        }
        return redirect(route('particular.index'))->with('message','l\'utilisateur particulier '.$user->name.' a été modifiée avec succès' );
    }


    public function destroy($id)
    {
        $user=User::find($id);
        $name=$user->name;
        $user->delete();

        return redirect(route('particular.index'))->with('message','l\'utilisateur particulier '.$name.' a été supprimée avec succès' );
    }
}
