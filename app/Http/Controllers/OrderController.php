<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public function index()
    {
        $factures=Bill::all();
        if(Auth::user()->company_id)
        {
            $order=new Order();
            $orders=$order->companyOrders(Auth::user()->company_id)->get();
        }

        else
            $orders=Order::all();
        return view('admin.order.index',compact('orders','factures'));
    }


    public function show(Order $order)
    {
        $total=$order->total();
        $products=json_decode($order->product);
        $order=Order::find($order->id)->first();

        return view('admin.order.show',compact('order','products','total'));
    }
    public function attente()
    {
        $factures=Bill::all();
        if(Auth::user()->company_id)
        {
            $order=new Order();
            $orders=$order->companyOrders(Auth::user()->company_id)->where('status_id',1)->get();
        }

        else
            $orders=Order::where('status_id',1)->get();
        return view('admin.order.attente',compact('orders','factures'));
    }
    public function cours()
    {
        $factures=Bill::all();
        if(Auth::user()->company_id)
        {
            $order=new Order();
            $orders=$order->companyOrders(Auth::user()->company_id)->where('status_id',2)->get();
        }

        else
            $orders=Order::where('status_id',2)->get();

        return view('admin.order.cours',compact('orders','factures'));
    }
    public function livre()
    {
        $factures=Bill::all();
        if(Auth::user()->company_id)
        {
            $order=new Order();
            $orders=$order->companyOrders(Auth::user()->company_id)->where('status_id',3)->get();
        }

        else
            $orders=Order::where('status_id',3)->get();

        return view('admin.order.livre',compact('orders','factures'));
    }
    public function _getform(Request $request)
    {
        $order=Order::find($request->order_id);
        return view('ajax.order-edit-form',compact('order'));
    }
    public function _getformfacture(Request $request)
    {
        $order=Order::find($request->order_id);
        return view('ajax.order-edit-facture',compact('order'));
    }
    public function uploadFacture(Request $request,$id)
    {
                $order=Order::find($id);
                foreach($request->file('file') as $file){
                    $facture=new Bill();
                    $imageName= date('yhmdsi').'.'.$file->getClientOriginalExtension();
                    $file->storeAs('public/pictures/order/'.$order->id.'/facture',$imageName);
                    $facture->name=$imageName;
                    $facture->order_id=$id;
                    $facture->save();
                }
                $preview[] = asset('storage/pictures/order/'.$order->id.'/facture/'.$facture->name);

        return[ 'initialPreview' => $preview];
//        $facture=new Bill();
//        $imageName= date('yhmdsi').'.'.$file->getClientOriginalExtension();
//        $file->storeAs('public/pictures/order/'.$order->id.'/facture',$imageName);
//        $order->facture=$imageName;
//        $facture->save();

    }
    public function deleteFacture($id)
    {
        $facture = Bill::find($id);
        unlink(storage_path('app/public/pictures/order/'.$facture->order_id.'/facture/'.$facture->name));
        $facture->delete();
        return response()->json(['deleted' => true]);
    }
    public function status(Request $request,$id)
    {
        $order=Order::find($id);
        if($request->has('status'))
            $order->status_id=$request->status;
        if($request->has('price'))
            $order->price=$request->price;
        $order->update();

        return redirect(route('order.index'));
    }
    public function edit(Order $order)
    {
        //
    }


    public function update(Request $request, Order $order)
    {
        $order->status=$request->status;
        if($request->price)
            $order->price=$request->price;
        $order->update();
        return redirect()->back();
    }

    public function destroy(Order $order)
    {
        //
    }
}
