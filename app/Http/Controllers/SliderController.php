<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Slider::orderBy('order')->get();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('image', function (Slider $slider) {
                    $picture=Storage::url('public/pictures/slider/'.$slider->image);
                    return $btn='<img src="'.$picture.'" class="avatar-xs m-1 rounded-circle">';
                })
                ->addColumn('active', function (Slider $slider) {

                   if($slider->active==1)
                        return '<span class="badge badge-success badge-pill ">
                                        activé
                                    </span>';
                    else
                        return '<span class="badge badge-danger badge-pill ">
                                        désactivé
                                    </span>';
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('slider.edit',$row->id).'" class="btn btn-info btn-xs"><i class="uil uil-pen"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action','active','image'])
                ->make(true);
        }
        return view('admin.slider.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider=new Slider;
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        $slider->description=$request->description;
        $file=$request->file('image');
        $imageName = date('yhmdsi') . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public/pictures/slider/', $imageName);
        $slider->image=$imageName;
        $slider->button_text=$request->button_text;
        $slider->button_link=$request->button_link;
        $slider->button_text2=$request->button_text2;
        $slider->button_link2=$request->button_link2;
        $slider->order=$request->order;
        $slider->save();

        return redirect()->route('slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        $slider->description=$request->description;

        if($request->file('image'))
        {
            Storage::delete('public/pictures/slider/'.$slider->image);
            $file = $request->file('image');
            $imageName = date('yhmdsi') . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/pictures/slider/', $imageName);
            $slider->image = $imageName;
        }

        $slider->button_text=$request->button_text;
        $slider->button_link=$request->button_link;
        $slider->button_text2=$request->button_text2;
        $slider->button_link2=$request->button_link2;
        $slider->order=$request->order;
        $slider->update();

        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
