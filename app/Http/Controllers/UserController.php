<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function edit($id)
    {
        $user=User::find($id);
        return view('admin.user.edit',compact('user'));
    }
    public function password(Request $request, $id)
    {
        $request->validate([
            'password' => 'required'
        ]);
        $user = User::find($id);
        $user->password=$request->password;
        $user->update();
        return redirect(route('backoffice'))->with('message','le mot de passe de '.$user->name.' a été modifiée avec succès' );
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
//            'adresse' => 'required'
        ]);

        $user = User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
//        $user->adresse=$request->adresse;
        $user->update();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user/',$imageName);
            $user->image=$imageName;
            $user->update();
        }
        return back()->with('message','admin modifiée avec succès');
    }
}
