<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use DataTables;
class CategoryController extends Controller
{

    public function index()
    {

        $categories =Category::all();
        return view('admin.category.index',compact('categories'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $category=new Category($request->except('image','description'));
        $category->save();
        if($file=$request->file('image'))
        {
            $imageName= $category->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/category/'.$category->id,$imageName);
            $category->image=$imageName;
            $category->save();
        }
        return redirect(route('category.index'));
    }


    public function show(Category $category)
    {
        //
    }


    public function edit(Category $category)
    {
        //
    }


    public function update(Request $request, Category $category)
    {
        if($file=$request->file('image'))
        {
            $imageName= $category->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/category/'.$category->id,$imageName);
            $category->image=$imageName;
            $category->update();
        }
        $category->update($request->except('image','description'));

//        $category->update();
        return redirect(route('category.index'));
    }


    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('category.index'));
    }
}
