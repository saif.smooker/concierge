<?php

namespace App\Http\Controllers;
use App\Company;
use App\Http\Controllers\Controller;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{

    public function index()
    {
        $users = User::role('company')->where('en_attente', 0)->get();
        $order = new Order();
        $today=Carbon::today();
        return view('admin.company.index',compact('users','order','today'));
    }

    public function create()
    {
        DB::table('users');
        return view('admin.company.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
            'adresse' => 'required',
            'status' => 'required',
            'nbr_employee' => 'required',
            'prix_employee' => 'required',
        ]);
        $company=new Company($request->except('image','password','email'));
        $company->prix=($request->nbr_employee)*($request->prix_employee);
//        dd($company->prix);
        $company->save();
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->adresse=$request->adresse;
        $user->password=$request->password;
        $user->company_id= $company->id;
        $user->prix=($request->nbr_employee)*($request->prix_employee);
        $user->date=date('Y:m:d');
        $user->assignRole('company');
        $user->save();
        if($file=$request->file('image'))
        {
            $imageName= $company->id.'.'.$file->getClientOriginalExtension();
            $imageName2= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/company',$imageName);
            $file->storeAs('public/pictures/user',$imageName2);
            $company->image=$imageName;
            $user->image=$imageName2;
            $company->save();
            $user->save();
        }
        return redirect(route('company.index'))->with('message','la société '.$company->name.' a été créée avec succès' );
    }

    public function show($id)
    {
        $company=Company::find($id);
//        $orders=Order::join('model_has_roles','model_has_roles.model_id','orders.user_id','users.company_id')
//            ->join('roles','roles.id','model_has_roles.role_id')
//            ->select('orders.*','roles.name','roles.id','model_has_roles.model_id','orders.user_id')
//            ->where('roles.name','company')->get();
            $order=new Order();
            $orderall=$order->companyOrders($id)->count();
            $ordersent=$order->companyOrders($id)->where('status_id','3')->count();

        return view('admin.company.show',compact('company','orderall','ordersent'));
    }


    public function edit($id)
    {

        $company=Company::find($id);
        $admin = User::role('company')->where('company_id',$company->id)->first();
        return view('admin.company.edit',compact('company','admin'));

    }

    public function password(Request $request, $id)
    {
        $request->validate([
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);
        $user = User::role('company')->where('company_id',$id)->first();
        $user->password=$request->password;
        $user->update();
        return redirect(route('company.index'))->with('message','le mot passe de '.$user->name.' a été modifiée avec succès' );
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'adresse' => 'required',
            'status' => 'required',
            'nbr_employee' => 'required',
            'prix_employee' => 'required',
        ]);
        $company=Company::find($id);
        if($file=$request->file('image'))

        $company->name=$request->name;
        $company->adresse=$request->adresse;
        $company->status=$request->status;

        $company->nbr_employee=$request->nbr_employee;
        $company->prix_employee=$request->prix_employee;
        $company->update();
        if($file=$request->file('image'))
        {
            $imageName= $company->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/company',$imageName);
            $company->image=$imageName;
            $company->update();
        }
        $user = User::role('company')->where('company_id',$id)->first();
        $user->name=$request->name;
        $user->email=$request->email;

        $user->company_id=$company->id;
        $user->update();
        return redirect(route('company.index'))->with('message','la société '.$company->name.' a été modifiée avec succès' );
    }

    public function attente()
    {
        $users = User::role('company')->where('en_attente', 1)->get();
        $order = new Order();
        $today=Carbon::today();
        return view('admin.company.attente',compact('users','order','today'));
    }
    public function destroy($id)
    {
        $company=Company::find($id);
        $name=$company->name;
        $user=User::role('company')->where('company_id',$id)->first();
        $user->delete();
        $company->delete();
        return redirect(route('company.index'))->with('message','la société '.$name.' a été supprimée avec succès' );
    }

    public function accept($company_id) {
        $company = Company::find($company_id);
        $company->status = 1;
        $user = User::where('company_id', $company->id)->first();
        $user->status = 1;
        $user->en_attente = 0;
        $company->save();
        $user->save();

        return redirect()->back()->with('message', 'Société Accepté');
    }
}

