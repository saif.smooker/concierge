<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductPicture;
use App\Type;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function index()
    {
        $products=Product::all();
        return view('admin.product.index',compact('products'));
    }


    public function create()
    {
        $categories=Type::all();
        $units=Unit::all();
        return view('admin.product.create',compact('units','categories'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'type' => 'required',
            'unit' => 'required',
            'status' => 'required',
        ]);

        $product=new Product();
        $product->name=$request->name;
        $product->description=$request->description;
        $product->price=$request->price;
        $product->status=$request->status;
        $product->type_id=$request->type;
        $product->unit_id=$request->unit;
        $product->save();
        if($request->file('image'))
        {
            $file=$request->file('image');
            $imageName= $product->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/product/'.$product->id,$imageName);
            $product->image=$imageName;
            $product->save();
        }
        return redirect(route('product.index'))->with('message','le produit '.$product->name.' a été crée avec succès' );
    }


    public function show(Product $product)
    {

        return view('admin.product.show',compact('product'));
    }


    public function edit(Product $product)
    {
        $categories=Type::all();
        $units=Unit::all();
        return view('admin.product.edit',compact('product','units','categories'));
    }


    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'type' => 'required',
            'unit' => 'required',
            'status' => 'required',
        ]);

        $product->name=$request->name;
        $product->description=$request->description;
        if($file=$request->file('image'))
        {
            $file=$request->file('image');
            $imageName= $product->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/product/'.$product->id,$imageName);
            $product->image=$imageName;
        }
        $product->status=$request->status;
        $product->price=$request->price;
        $product->type_id=$request->type;
        $product->unit_id=$request->unit;
        $product->update();
        return redirect(route('product.index'))->with('message','le produit '.$product->name.' a été modifiée avec succès' );
    }

    public function pictures($id)
    {
        $pictures=ProductPicture::where('product_id',$id)->get();
        $product=Product::find($id);
        return view('admin.product.picture',compact('pictures','product'));
    }
    public function picturesupload(Request $request,$id)
    {
        $picture=new ProductPicture();

            $file=$request->file('file') ;
//            dd($file);
            $imageName = date('yhmdsi') . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/pictures/product/'.$id.'/images', $imageName);
            $picture->name = $imageName;
            $picture->product_id = $id;
            $picture->save();
        $config[] = [
            'key' => $picture->id,
            'caption' => $picture->name,
            'url' => route('product.pictures.delete',[$id,$picture->id]), // server api to delete the file based on key
        ];
        $preview[] = asset('storage/pictures/product/'.$id.'/images/'.$picture->name);
        return[ 'initialPreview' => $preview, 'initialPreviewConfig' => $config];
    }
    public function picturesdelete(Request $request,$id,ProductPicture $picture)
    {
        unlink(storage_path('app/public/pictures/product/'.$id.'/images/'.$picture->name));
        $picture->delete();
        return response()->json(['deleted' => $picture->name]);
    }

    public function destroy(Product $product)
    {
        $name=$product->name;
        $product->delete();
        return redirect(route('product.index'))->with('message','le produit '.$name.' a été supprimée avec succès' );
    }
}
