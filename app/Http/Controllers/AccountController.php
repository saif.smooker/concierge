<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
class AccountController extends Controller
{
//    public function index()
//    {
////        $services=Service::all();
////        $categories=Category::all();
//        $orders=Auth::user()->orders()->latest()->get();
//        return view('front.user.index',compact('orders'));
//    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Auth::user()->orders()->orderBy('created_at')->get();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('date', function (Order $order) {
                    return $order->created_at->diffForHumans();
                })
                ->addColumn('name', function (Order $order) {
                    return $order->service->name;
                })
                ->addColumn('prix', function (Order $order) {
                    if($order->product)
                    return $order->total().'DT';

                    elseif($order->facture){
                      $btn='<button class="btn btn-light btn-sm" data-toggle="modal"
                              data-target="#modal-facture{{$order->id}}">Voir Facture</button>';
                    return $btn;}
                })
                ->addColumn('status', function (Order $order) {

                    if($order->status_id==1)
                         return '<span class="badge badge-warning" >En attente</span>';
                    elseif($order->status_id==2)
                        return '<span  class="badge badge-info" >En cours</span>';
                    else
                        return  '<span class="badge badge-success" >Livré</span>';

                })
                ->addColumn('action', function($row){
                    $btn = '<a class="btn btn-light btn-sm" href="'.route('compte.order',$row->id).'">Voir Commande</a>';
                    return $btn;
                })
                ->rawColumns(['action','status','prix'])
                ->make(true);
        }
        return view('front.user.index');
    }
    public function edit()
    {

        $services=Service::all();
        $categories=Category::all();
        return view('front.user.edit',compact('services','categories'));
    }
    public function update(Request $request)
    {
        $user=User::find(Auth::user()->id);
        if($request->has('name'))
        $user->name=$request->name;
        if($request->has('email'))
        $user->email=$request->email;
        if($request->has('adresse'))
        $user->adresse=$request->adresse;
        if($request->has('password'))
        $user->password=$request->password;
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user/',$imageName);
            $user->image=$imageName;
        }
        $user->update();

        return redirect(route('compte.edit'));
    }
    public function order()
    {
        $services=Service::all();
        $categories=Category::all();
        return view('front.user.index',compact('services','categories'));
    }

}
