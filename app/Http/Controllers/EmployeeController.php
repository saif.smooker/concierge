<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $users=User::query();
        $role=Auth::user()->getRoleNames()->first();

        if ($role=='company') {
            $users_company =$users->where('company_id','=',Auth::user()->company_id);
            $users = $users_company->role('employee');
        }
        else
        {
            $users = $users->role('employee');
        }
        if ($request->company_id)
        {
            $users = $users->where('company_id', $request->company_id);
        }
        if ($request->search)
        {
            $term = $request->search;
            $users = $users->where(function($query) use ($term) {
                $query->orWhere('name', 'like', '%' . $term . '%')
                    ->orWhere('email', 'like', '%' . $term . '%')
                    ->orWhere('adresse', 'like', '%' . $term . '%');
            });
        }
        $users = $users->paginate(5);
        return view('admin.employee.index',compact('users'));
    }


    public function create()
    {
        if (!Auth::user()->canadd())
        {
            return redirect(route('employee.index'))->with('message','vous avez atteint le nombre maximale d\'utilisateurs ');
        }
        $role=Auth::user()->getRoleNames()->first();
        if($role=="company")
            $companies=Company::where('id',Auth::user()->company_id)->first();
        else
            $companies=Company::all();
        return view('admin.employee.create',compact('companies'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'adresse' => 'required',
            'status' => 'required',
        ]);
        $role=Auth::user()->getRoleNames()->first();
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->adresse=$request->adresse;
        $user->password=bcrypt($request->password);
        $user->status=$request->status;
        if($role=="company")
        $user->company_id=Auth::user()->company_id;
        else
        $user->company_id=$request->company;
        $user->assignRole('employee');
        $user->save();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user',$imageName);
            $user->image=$imageName;
            $user->save();
        }
        return redirect(route('employee.index'));
    }


    public function show($id)
    {
        $user = User::find($id);
        return view('admin.employee.show',compact('user'));
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.employee.edit',compact('user'));
    }
    public function password(Request $request, $id)
    {
        $request->validate([
            'password' => 'required'
        ]);
        $user = User::find($id);
        $user->password=$request->password;
        $user->update();
        return redirect(route('employee.index'))->with('message','le mot de passe de '.$user->name.' a été modifiée avec succès' );
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
//            'password' => 'required',
            'adresse' => 'required',
            'status' => 'required',
//            'company' => 'required',
        ]);
        $user = User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->adresse=$request->adresse;
//        $user->password=$request->password;
        $user->status=$request->status;
        if($request->company)
        $user->company_id=$request->company;

        $user->update();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user',$imageName);
            $user->image=$imageName;
            $user->update();
        }
        return redirect(route('employee.index'))->with('message','l\'utilisateur '.$user->name.' a été modifiée avec succès' );
    }


    public function destroy($id)
    {
        if(Auth::user()->getRoleNames()->first()=="company")
        {
            $user= User::find($id);
            $user->status=0;
            $user->update();
        }
        else
        User::find($id)->delete();
        return redirect(route('employee.index'));
    }
}
