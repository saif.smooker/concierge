<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class WorkerController extends Controller
{

    public function index()
    {
        $users = User::role('worker')->get();
        return view('admin.worker.index',compact('users'));
    }


    public function create()
    {
        return view('admin.worker.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);

        $user->assignRole('worker');
        $user->save();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user/',$imageName);
            $user->image=$imageName;
            $user->save();
        }
        return redirect(route('worker.index'))->with('message','l\'utilisateur '.$user->name.' a été crée avec succès' );
    }


    public function show($id)
    {
        $user = User::find($id);
        return view('admin.worker.show',compact('user'));
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.worker.edit',compact('user'));
    }
    public function password(Request $request, $id)
    {
        $request->validate([
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);
        $user = User::find($id);
        $user->password=$request->password;
        $user->update();
        return redirect(route('worker.index'))->with('message','le mot de passe de '.$user->name.' a été modifiée avec succès' );
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'adresse' => 'required'
        ]);

        $user = User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->adresse=$request->adresse;
        $user->update();
        if($file=$request->file('image'))
        {
            $imageName= $user->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/user/',$imageName);
            $user->image=$imageName;
            $user->update();
        }
        return redirect(route('worker.index'))->with('message','l\'utilisateur '.$user->name.' a été crée avec succès' );
    }


    public function destroy($id)
    {
        $user=User::find($id);
        $name=$user->name;
        $user->delete();
       return redirect(route('worker.index'))->with('message','l\'utilisateur '.$name.' a été supprimée avec succès' );
    }
}
