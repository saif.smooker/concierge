<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;
use App\Service;
use App\ServicePicture;
use App\Slider;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function account()
    {
        $services=Service::all();
        $categories=Category::all();
        return view('front.user.index',compact('services','categories'));
    }
    public function index()
    {
        $sliders=Slider::where('active','1')->orderBy('order')->get();
        $services=Service::all();
        $categories=Category::all();
//        dd($categories);
        return view('front.home',compact('services','categories','sliders'));
    }
    public function about()
    {

        return view('front.about');
    }
    public function contact()
    {

        return view('front.contact');
    }
    public function list(Category $category)
    {

        if(!Auth::user())
        $services=$category->services()->where('status','=',1)->get();
        else
        {
            $user=User::find(Auth::user()->id);
            $role=$user->getRoleNames()->first();
            $filtered=$category->services()->where('status','=',1)->get();
            $service = $filtered->filter(function ($value, $key) use($role){
                if (Str::contains($value->permission,$role))
                return $value;
//                return $value > 2;
            });
            $services=$service->all();
//
        }

        $categories=Category::all();
        return view('front.service.list',compact('category','services','categories'));
    }
    public function detail(Service $service)
    {
//        $service = Service::find($id);
        $can=0;
        if(Auth::user())
        {
            $user=User::find(Auth::user()->id);
            $role=$user->getRoleNames()->first();
            if (Str::contains($service->permission,$role))
                $can=1;
            else
                $can=0;
        }

        $types=$service->types->pluck('id')->toArray();
        $products=Product::whereIn('type_id',$types)->get();
        $days = array_map('intval', explode(',', $service->dateopen));
        $services=Service::where('id','!=',$service->id)->where('status',1)->where('category_id',$service->category->id)->get();

//        dd($services);
//        $pictures=ServicePicture::where('service_id',$service->id)->get();
//        dd($service->id);
//        dd($can);
        return view('front.service.detail',compact('service','days','products','services','can'));
    }
    public function dashboard()
    {
        if(Auth::user()->company_id)
        {
            $order=new Order();
            $attente=$order->companyOrders(Auth::user()->company_id)->where('status_id',1)->count();
            $encours=$order->companyOrders(Auth::user()->company_id)->where('status_id',2)->count();
            $livre=$order->companyOrders(Auth::user()->company_id)->where('status_id',3)->count();
        }

        else
        {
            $attente=Order::where('status_id',1)->count();
            $encours=Order::where('status_id',2)->count();
            $livre=Order::where('status_id',3)->count();
        }

        if(Auth::user()->getRoleNames()->contains('company'))
            $orders=Order::join('users','users.id','orders.user_id')
                ->select('orders.*')
                ->where('users.company_id',Auth::user()->company_id)
                ->where('status_id','1')
                ->take(5)
                ->get();
        else
            $orders=Order::orderBy('id', 'desc')->take(5)->get();

        return view('admin.dashboard',compact('orders','attente','encours','livre'));
    }

    public function _getservices(Request $request) {
        $services = Service::where('category_id', $request->category_id)->get();
        return view('ajax.getservices', compact('services'));
    }
}
