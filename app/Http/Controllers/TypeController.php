<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{

    public function index()
    {
        $types=Type::all();
        return view('admin.type.index',compact('types'));
    }


    public function create()
    {
        //
    }



    public function store(Request $request)
    {
        $type=new Type($request->except('image'));
            if($file=$request->file('image'))
            {
                $imageName= date('ydhmsi').'.'.$file->getClientOriginalExtension();
                $file->storeAs('public/pictures/type',$imageName);
                $type->image=$imageName;
            }
        $type->save();
        return redirect(route('type.index'))->with('message','la catégorie '.$type->name.' a été crée avec succès' );
    }


    public function show(Type $type)
    {
        //
    }


    public function edit(Type $type)
    {
        //
    }

    public function update(Request $request, Type $type)
    {
        $type->fill($request->except('image'));
        if($file=$request->file('image'))
        {
            $imageName= date('ydhmsi').'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/type',$imageName);
            $type->image=$imageName;
        }
        $type->update();
        return redirect(route('type.index'))->with('message','la catégorie '.$type->name.' a été modifiée avec succès' );;
    }


    public function destroy(Type $type)
    {
        $type->delete();
        return redirect(route('type.index'))->with('message','la catégorie '.$type->name.' a été supprimée avec succès' );;
    }
}
