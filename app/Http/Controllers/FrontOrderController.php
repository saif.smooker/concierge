<?php

namespace App\Http\Controllers;

use App\Document;
use App\Order;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Sopamo\LaravelFilepond\Filepond;

class FrontOrderController extends Controller
{

    public function show(Order $order)
    {
        $total=$order->total();
        $products=json_decode($order->product);
        return view('front.user.order',compact('order','total','products'));
    }

    public function store(Request $request,Service $service)
    {
//        $date = explode('/', $request->date);
//        $date=str_replace("/","-",$request->date);
//        $date3=Carbon::parse($date)->format('Y-m-d');
//        dd($date3);

        $order=new Order();
        $order->code=Str::random(6);
        $order->user_id=Auth::user()->id;

        if($service->besoin)
        {
            $order->besoin=$request->besoin;
        }

        if($service->date)
        {
            $validation['date'] = 'required';
            if($service->multidate==1)
            {
                $dates = explode(' - ', $request->date);
                $datedeb=str_replace("/","-",$dates[0]);
                $order->datedeb = Carbon::parse($datedeb)->format('Y-m-d');
                $datefin=str_replace("/","-",$dates[1]);
                $order->datefin = Carbon::parse($datefin)->format('Y-m-d');
            }
            else{

            $date=str_replace("/","-",$request->date);
            $date=Carbon::parse($date)->format('Y-m-d');
            if ($date<Carbon::now()->format('Y-m-d'))
                return redirect()->back()->with('message','la date sélectionnée est inadmissible' );
            $order->datedeb=$date;}
        }

        if($service->product)
        {
            $validation['product'] = 'required';
            $products=$request->products;
            $order->product=json_encode($products);
        }
        $order->service_id=$service->id;
        $order->status_id=1;
        $order->save();
        if($service->document)
        {
            $validation['documents'] = 'required';
            $files=$request->file('documents');
            foreach($files as $file)
            {
                $document=new Document();
                $imageName = date('yhdmsi').'.'.$file->getClientOriginalExtension();
                $file->storeAs('public/pictures/order/'.$order->id.'/document',$imageName);
                $document->order_id=$order->id;
                $document->name=$imageName;
                $document->save();
            }
        }
        return redirect(route('compte'))->with('message','la commande du service '.$service->name.' a été créée avec succès' );
    }
}
