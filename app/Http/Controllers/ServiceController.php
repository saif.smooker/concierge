<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use App\ServicePicture;
use App\Type;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function index()
    {
        $services=Service::all();
        return view('admin.service.index',compact('services'));
    }

    public function create()
    {
        $categories=Category::all();
        $types=Type::all();
        return view('admin.service.create',compact('categories','types'));
    }

    public function store(Request $request)
    {
        $validation =[
            'name' => 'required',
            'description' => 'required',
            'status' => 'required',
            'category' => 'required',
            'permission' => 'required',
        ];

//        if ( $request->has('date') ) {
//            $validation['dateopen'] = 'required';
//        } else
            if ($request->has('product')) {
            $validation['type'] = 'required';
        } elseif (!$request->document && !$request->besoin) {
            $validation['besoin'] = 'required';
        }
        $request->validate($validation,['besoin.required' => 'Au moin un bouton doit etre coché']);

        $service=new Service();
        $service->name=$request->name;
        $service->description=$request->description;
        $service->besoin=($request->has('besoin') ? 1 : 0);
        $service->document=($request->has('document') ? 1 : 0);
        $service->multidate=($request->has('multidate') ? 1 : 0);
        $service->date=($request->has('date') ? 1 : 0);
        $service->product=($request->has('product')? 1 : 0);
        $service->status=$request->status;
        if($request->has('dateopen'))
        {
            $service->dateopen = implode(',', $request->dateopen);
        }
        $service->permission = implode(',', $request->permission);
        $service->category_id=$request->category;
        $service->save();
        if($request->has('product'))
        {
            $service->types()->attach($request->type);
            $service->save();
        }
        if($file=$request->file('image'))
        {
            $imageName= $service->id.'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/service/'.$service->id,$imageName);
            $service->image=$imageName;
            $service->save();
        }
        return redirect(route('service.index'))->with('message','le service '.$service->name.' a été crée avec succès' );
    }

    public function show(Service $service)
    {
        $types=Type::all();
        $categories=Category::all();
        return view('admin.service.show',compact('service','categories','types'));
    }


    public function edit(Service $service)
    {
        $types=Type::all();
        $categories=Category::all();
        return view('admin.service.edit',compact('service','categories','types'));
    }


    public function update(Request $request, Service $service)
    {
        $validation =[
            'name' => 'required',
            'description' => 'required',
            'status' => 'required',
            'category' => 'required',
            'permission' => 'required',
        ];
//            dd(implode(',', $request->permission));
//        if ( $request->has('date') ) {
//            $validation['dateopen'] = 'required';
//        } else
            if ($request->has('product')) {
            $validation['type'] = 'required';
        } elseif (!$request->document && !$request->besoin) {
            $validation['besoin'] = 'required';
        }
        $request->validate($validation,['besoin.required' => 'Au moin un bouton doit etre coché']);

        $service->name=$request->name;
        $service->description=$request->description;
        $service->besoin=($request->has('besoin') ? 1 : 0);
        $service->document=($request->has('document') ? 1 : 0);
        $service->date=($request->has('date') ? 1 : 0);
        $service->multidate=($request->has('multidate') ? 1 : 0);
        $service->product=($request->has('product') ? 1 : 0);
        $service->category_id=$request->category;
        $service->status=$request->status;
        if($request->has('dateopen'))
        {
            $service->dateopen = implode(',', $request->dateopen);
        }
            $service->permission = implode(',', $request->permission);
        if($request->has('product'))
        {
            $service->types()->sync($request->type);
        }
        $service->update();
        if($file=$request->file('image'))
        {
            $imageName= date('ydhmsi').'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/pictures/service/'.$service->id,$imageName);
            $service->image=$imageName;
            $service->update();
        }
        return redirect(route('service.index'))->with('message','le service '.$service->name.' a été modifiée avec succès' );
    }

    public function pictures($id)
    {
        $pictures=ServicePicture::where('service_id',$id)->get();
        $service=Service::find($id);
        return view('admin.service.picture',compact('pictures','service'));
    }
    public function picturesupload(Request $request,$id)
    {
        $picture=new ServicePicture();

        $file=$request->file('file') ;
//            dd($file);
        $imageName = date('yhmdsi') . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public/pictures/service/'.$id.'/images', $imageName);
        $picture->name = $imageName;
        $picture->service_id = $id;
        $picture->save();
        $config[] = [
            'key' => $picture->id,
            'caption' => $picture->name,
            'url' => route('service.pictures.delete',[$id,$picture->id]), // server api to delete the file based on key
        ];
        $preview[] = asset('storage/pictures/service/'.$id.'/images/'.$picture->name);
        return[ 'initialPreview' => $preview, 'initialPreviewConfig' => $config];
    }
    public function picturesdelete(Request $request,$id,ServicePicture $picture)
    {
        unlink(storage_path('app/public/pictures/service/'.$id.'/images/'.$picture->name));
        $picture->delete();
        return response()->json(['deleted' => $picture->name]);
    }
    public function destroy(Service $service)
    {
        $name=$service->name;
        $service->delete();
        return redirect(route('service.index'))->with('message','le service '.$name.' a été supprimée avec succès' );
    }
}
