<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $guarded=[];

    public function orders(){
        return $this->belongsTo('App\Order');
    }
}
