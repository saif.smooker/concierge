<?php

namespace App\Providers;

use App\Category;
use App\Service;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('fr');
        Builder::defaultStringLength(191);
        view()->composer([
            'layouts.front',
        ], function ($view) {
            $view->with('cat', $this->getCats());
        });
    }
    protected function getCats(){
        if (!Auth::check())
             return Category::all();
        else
            {
                $user=User::find(Auth::user()->id);
                $role=$user->getRoleNames()->first();
                $services=Service::where('permission','LIKE','%'.$role.'%')->get();
                 return Category::join('services','categories.id','services.category_id')
                     ->select('categories.*')
                    ->where('services.permission','LIKE','%'.$role.'%')
                    ->groupBy('services.category_id')
                    ->get();


            }
    }
}
