<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=[];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

}
