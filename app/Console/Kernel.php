<?php

namespace App\Console;

use App\Payment;
use Carbon\Carbon;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            $users=User::role('company')->get();
            $today=Carbon::today();
            foreach ($users as $user)
            {

                if ($user->date<=today())
                {
                    $payment=new Payment();
                    $payment->date=$user->date;
                    $payment->user_id=$user->id;
                    $payment->mail=0;
                    $payment->save();

                }
            }
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
