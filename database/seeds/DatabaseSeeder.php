<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);//
        Role::create(['name' => 'worker']);//

        Role::create(['name' => 'company']);//
        Role::create(['name' => 'employee']);//

        Role::create(['name' => 'particular']);//

        DB::table('users')->insert([
            'name' => 'User1',
            'email' => 'user@gmail.com',
            'phone' =>'21555333',
            'password' => bcrypt('password'),
        ]);

        $user=User::find(1);
        $user->assignRole('admin');

        DB::table('statuses')->insert([
            'name' => 'en attente',
        ]);
        DB::table('statuses')->insert([
            'name' => 'en cours',
        ]);
        DB::table('statuses')->insert([
            'name' => 'Livré',
        ]);
        DB::table('categories')->insert([
            'name' => 'alimentation',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'A domicile',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Enfants',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Voiture',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Administratif',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Et plus',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Fournitures',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Evénement',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'International',
            'status'=>1
        ]);
        DB::table('categories')->insert([
            'name' => 'Courrier',
            'status'=>1
        ]);
        DB::table('services')->insert([
            'name' => 'Repas « fait maison » à emporter',
            'description' => 'Repas « fait maison » à emporter',
            'besoin' => '1',
            'status'=>1,
            'category_id'=>1
        ]);
        DB::table('services')->insert([
            'name' => 'Livraison de courses',
            'description' => 'Livraison de courses',
            'besoin' => '1',
            'status'=>1,
            'category_id'=>1
        ]);
        DB::table('services')->insert([
            'name' => 'Autres',
            'description' => 'Autres',
            'besoin' => '1',
            'status'=>1,
            'category_id'=>1
        ]);
        DB::table('services')->insert([
            'name' => 'Médicament',
            'description' => 'Médicament',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>1
        ]);
        DB::table('services')->insert([
            'name' => 'Ménage, repassage',
            'description' => 'Ménage, repassage',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Jardinage, bricolage',
            'description' => 'Jardinage, bricolage',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Aide aux personnes dépendantes',
            'description' => 'Aide aux personnes dépendantes',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Assistance informatique',
            'description' => 'Assistance informatique',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Garde d’animaux',
            'description' => 'Garde d’animaux',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Clés à refaire',
            'description' => 'Clés à refaire',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Déménagement',
            'description' => 'Déménagement',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Recherche d’artisans',
            'description' => 'Recherche d’artisans',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Transport à la demande',
            'description' => 'Transport à la demande',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>2
        ]);
        DB::table('services')->insert([
            'name' => 'Soutien scolaire à domicile ou en ligne',
            'description' => 'Soutien scolaire à domicile ou en ligne',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>3
        ]);
        DB::table('services')->insert([
            'name' => 'Colis de fournitures scolaires',
            'description' => 'Colis de fournitures scolaires',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>3
        ]);
        DB::table('services')->insert([
            'name' => 'Pressing, blanchisserie, repassage',
            'description' => 'Pressing, blanchisserie, repassage',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>4
        ]);
        DB::table('services')->insert([
            'name' => 'Pressing, blanchisserie, repassage',
            'description' => 'Pressing, blanchisserie, repassage',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>4
        ]);
        DB::table('services')->insert([
            'name' => 'Retouches',
            'description' => 'Retouches',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>4
        ]);
        DB::table('services')->insert([
                'name' => 'Cordonnerie ',
                'description' => 'Cordonnerie ',
                'besoin' => '1',
                'document' => '1',
                'status'=>1,
                'category_id'=>4
            ]);
        DB::table('services')->insert([
            'name' => 'Pile de montre, bijoux',
            'description' => 'Pile de montre, bijoux',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>4
        ]);
        DB::table('services')->insert([
            'name' => 'Don de linge à des associations',
            'description' => 'Don de linge à des associations',
            'besoin' => '1',
            'document' => '1',
            'status'=>1,
            'category_id'=>4
        ]);
        DB::table('services')->insert([
                'name' => 'Nettoyage de couette ou de tapis',
                'description' => 'Nettoyage de couette ou de tapis',
                'besoin' => '1',
                'document' => '1',
                'status'=>1,
                'category_id'=>4
        ]);

        DB::table('units')->insert([
            'type' => 'kilo',
        ]);
        DB::table('units')->insert([
            'type' => 'piéce',
        ]);
        DB::table('units')->insert([
            'type' => 'litre',
        ]);

        DB::table('types')->insert([
            'name' => 'légumes',
        ]);
        DB::table('products')->insert([
            'name' => 'légumes',
            'description' => 'légumes',
            'type_id' => '1',
            'unit_id' => '1',
        ]);
    }
}
