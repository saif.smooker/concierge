<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->nullable();
            $table->string('name');
            $table->string('short')->nullable();
            $table->text('description');
            $table->boolean('status')->default(0);
            $table->boolean('besoin')->nullable();
            $table->boolean('document')->nullable();
            $table->boolean('date')->nullable();
            $table->boolean('multidate')->default(0);
            $table->string('dateopen')->nullable();
            $table->string('product')->nullable();
            $table->string('permission')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
