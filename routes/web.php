<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('list/{category}', 'HomeController@list')->name('service.list');
Route::get('/', 'HomeController@index')->name('home');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact', 'HomeController@contact')->name('contact');
//Route::group(['middleware' => 'role:admin|worker|company|employee'], function () {
    Route::get('service/{service}', 'HomeController@detail')->name('service.detail');
//});
Auth::routes();
Route::get('mon-compte','AccountController@index')->name('compte');
Route::get('mon-compte/order/{order}','FrontOrderController@show')->name('compte.order');
Route::get('mon-compte/edit','AccountController@edit')->name('compte.edit');
Route::put('mon-compte/edit','AccountController@update')->name('compte.update');
Route::post('service/{service}','FrontOrderController@store')->name('front.order.store');
Route::group(['middleware' => 'role:admin','prefix'=>'backoffice'], function () {
Route::get('user/{id}/edit', 'UserController@edit')->name('user.edit');
Route::put('user/{id}/password', 'UserController@password')->name('user.password');
Route::put('user/{id}/edit', 'UserController@update')->name('user.update');
});
Route::group(['middleware' => ['role:admin|worker'],'prefix'=>'backoffice'], function () {

   Route::get('company/attente', 'CompanyController@attente')->name('company.attente');
   Route::get('company/accept/{company_id}', 'CompanyController@accept')->name('company.accept');
    Route::get('particular/attente', 'ParticularController@attente')->name('particular.attente');
    Route::get('particular/accept/{user_id}', 'ParticularController@accept')->name('particular.accept');
    Route::resource('company', 'CompanyController');
    Route::put('company/{id}/edit', 'CompanyController@password')->name('company.password');

    Route::resource('particular', 'ParticularController');
    Route::put('particular/{id}/edit', 'ParticularController@password')->name('particular.password');

    Route::put('order/{id}', 'OrderController@status')->name('order.status');


    Route::resource('service', 'ServiceController');
    Route::get('service/{id}/pictures', 'ServiceController@pictures')->name('service.pictures');
    Route::post('service/{id}/pictures', 'ServiceController@picturesupload')->name('service.pictures.upload');
    Route::delete('service/{id}/picture/{picture}', 'ServiceController@picturesdelete')->name('service.pictures.delete');

    Route::resource('category', 'CategoryController');

    Route::resource('product', 'ProductController');

    Route::get('product/{id}/pictures', 'ProductController@pictures')->name('product.pictures');
    Route::post('product/{id}/pictures', 'ProductController@picturesupload')->name('product.pictures.upload');
    Route::delete('product/{id}/picture/{picture}', 'ProductController@picturesdelete')->name('product.pictures.delete');

    Route::resource('type', 'TypeController');

    Route::put('/ajax/editorder','OrderController@_getform')->name('ajax.editform');
    Route::post('/ajax/editfacture','OrderController@_getformfacture')->name('ajax.editfacture');
    Route::post('/uploadfacture/{id?}','OrderController@uploadFacture')->name('upload.facture');
    Route::delete('/deltefacture/{id?}','OrderController@deleteFacture')->name('delete.facture');

    Route::resource('worker', 'WorkerController');
    Route::put('worker/{id}/edit', 'WorkerController@password')->name('worker.password');

    Route::resource('payment', 'PaymentController');
});

Route::group(['middleware' => ['role:admin|worker|company'],'prefix'=>'backoffice'], function () {
    Route::get('order/attente', 'OrderController@attente')->name('order.attente');
    Route::get('order/cours', 'OrderController@cours')->name('order.cours');
    Route::get('order/livre', 'OrderController@livre')->name('order.livre');
    Route::resource('order', 'OrderController');


    Route::resource('employee', 'EmployeeController');
    Route::put('employee/{id}/edit', 'EmployeeController@password')->name('employee.password');

    Route::get('/', 'HomeController@dashboard')->name('backoffice');
});
Route::group(['middleware' => 'role:admin'], function () {
    Route::resource('slider', 'SliderController');
});
Route::group(['middleware' => 'role:admin|worker|company'], function () {
    Route::get('pages-logout', 'RoutingController@logout');
//    Route::get('/', 'RoutingController@index');
    Route::get('{first}/{second}/{third}', 'RoutingController@thirdLevel');
    Route::get('{first}/{second}', 'RoutingController@secondLevel');
    Route::get('{any}', 'RoutingController@root');
//    Route::get('/backoffice', 'HomeController@backoffice')->name('backoffice');
});

Route::post('/ajax/getservices','HomeController@_getservices')->name('ajax.getservices');

//Route::prefix('api')->group(function () {
//    Route::post('/process', 'FilepondController@upload')->name('filepond.upload');
//});
